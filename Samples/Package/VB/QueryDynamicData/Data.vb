﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports IPS7Lnk.Advanced
Namespace QueryDynamicData
    Public Class Data
        Inherits PlcObject
        Private Shared m_dataBlockNumber As Integer = 1

        Private m_byteValue As PlcByte
        Private m_int16Value As PlcInt16
        Private m_int32Value As PlcInt32
        Private m_realValue As PlcReal
        Private m_stringValue As PlcString

        Public Sub New()
            MyBase.New()
            ' The following lines initialize the local PLC specific members using the
            ' different PLC types to generate the dynamic members of the PLC Object.
            ' While the PlcByte, PlcInt16, etc. are initialized with a dynamically
            ' created PLC address which uses a static member to identify the number
            ' of the DataBlock to address within the new instance.

            ' DBx.DBB 2
            Me.m_byteValue = New PlcByte(New PlcAddress( _
                    PlcOperand.DataBlock(Data.DataBlockNumber), PlcRawType.[Byte], 2))
            Me.Members.Add("ByteValue", Me.m_byteValue)

            ' DBx.DBW 4
            Me.m_int16Value = New PlcInt16(New PlcAddress( _
                    PlcOperand.DataBlock(Data.DataBlockNumber), PlcRawType.Word, 4))
            Me.Members.Add("Int16Value", Me.m_int16Value)

            ' DBx.DBD 6
            Me.m_int32Value = New PlcInt32(New PlcAddress( _
                    PlcOperand.DataBlock(Data.DataBlockNumber), PlcRawType.DWord, 6))
            Me.Members.Add("Int32Value", Me.m_int32Value)

            ' DBx.DBD 10
            Me.m_realValue = New PlcReal(New PlcAddress( _
                    PlcOperand.DataBlock(Data.DataBlockNumber), PlcRawType.DWord, 10))
            Me.Members.Add("RealValue", Me.m_realValue)

            ' DBx.DBB 20
            Me.m_stringValue = New PlcString(New PlcAddress( _
                    PlcOperand.DataBlock(Data.DataBlockNumber), PlcRawType.[Byte], 20), 16)
            Me.Members.Add("StringValue", Me.m_stringValue)
        End Sub

        Public Shared Property DataBlockNumber() As Integer
            Get
                Return Data.m_dataBlockNumber
            End Get
            Set(value As Integer)
                Data.m_dataBlockNumber = value
            End Set
        End Property

        Public Property ByteValue() As Byte
            Get
                Return Me.m_byteValue.Value
            End Get
            Set(value As Byte)
                Me.m_byteValue.Value = value
            End Set
        End Property

        Public Property Int16Value() As Short
            Get
                Return Me.m_int16Value.Value
            End Get
            Set(value As Short)
                Me.m_int16Value.Value = value
            End Set
        End Property

        Public Property Int32Value() As Integer
            Get
                Return Me.m_int32Value.Value
            End Get
            Set(value As Integer)
                Me.m_int32Value.Value = value
            End Set
        End Property

        Public Property RealValue() As Single
            Get
                Return Me.m_realValue.Value
            End Get
            Set(value As Single)
                Me.m_realValue.Value = value
            End Set
        End Property

        Public Property StringValue() As String
            Get
                Return Me.m_stringValue.Value
            End Get
            Set(value As String)
                Me.m_stringValue.Value = value
            End Set
        End Property
    End Class
End Namespace
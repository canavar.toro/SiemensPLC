﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports IPS7Lnk.Advanced

Namespace PlcToCsv
    Public Class WeatherData
        Inherits PlcObject

        <PlcMember("DB111.DBB 2")> _
        Public ChanceOfRain As Byte

        <PlcMember("DB111.DBW 4")> _
        Public WindSpeed As Short

        <PlcMember("DB111.DBD 6")> _
        Public Pressure As Integer

        <PlcMember("DB111.DBD 10")> _
        Public Temperature As Single

        <PlcMember("DB111.DBB 20", Length:=16)> _
        Public Forecast As String
    End Class
End Namespace


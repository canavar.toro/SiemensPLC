﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports IPS7Lnk.Advanced

Namespace SqlToPlc
    Public Class PrintJobData
        Inherits PlcObject

        <PlcMember("DB111.DBB 2")> _
        Public NumberOfPages As Byte

        <PlcMember("DB111.DBW 4")> _
        Public Resolution As Short

        <PlcMember("DB111.DBD 6")> _
        Public LineHeight As Integer

        <PlcMember("DB111.DBD 10")> _
        Public Price As Single

        <PlcMember("DB111.DBB 20", Length:=16)> _
        Public ArticleNumber As String

        <PlcMember("DB111.DBX 1.0")> _
        Public ReadOnly StartPrint As Boolean = True
    End Class
End Namespace

﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports System
Imports System.IO
Imports System.Threading

Imports IPS7Lnk.Advanced

Namespace CsvToPlc
    ''' <summary>
    ''' This sample demonstrates a price label printing application which uses a CSV file as data
    ''' source of the information required to print the price labels.
    ''' </summary>
    ''' <remarks>
    ''' This application does read the print job information from a CSV file, writes the job
    ''' configuration to the PLC and when waits until the job has been processed by the PLC.
    ''' </remarks>
    Public Class Program
        Public Shared Sub Main(args As String())
            Dim device As New SiemensDevice( _
                    New IPDeviceEndPoint("192.168.0.80"), SiemensDeviceType.S7300_400)

            Dim connection As PlcDeviceConnection = device.CreateConnection()
            connection.Open()

            ' 1. Way: Sequential Write.
            If True Then
                ' Either use the primitive low level methods of the PLC device connection to
                ' sequential write the data.

                For Each line As String In File.ReadAllLines(".\Data.csv")
                    Dim values As String() = line.Split(";"c)

                    ' Number of pages.
                    connection.WriteByte("DB111.DBB 2", Convert.ToByte(values(0)))

                    ' Resolution in dpi.
                    connection.WriteInt16("DB111.DBW 4", Convert.ToInt16(values(1)))

                    ' Line Height in pixels.
                    connection.WriteInt32("DB111.DBD 6", Convert.ToInt32(values(2)))

                    ' Price.
                    connection.WriteReal("DB111.DBD 10", Convert.ToSingle(values(3)))

                    ' Article Number.
                    connection.WriteString("DB111.DBB 20", values(4))

                    connection.WriteBoolean("DB111.DBX 1.0", True)

                    ' Wait while printing.
                    While connection.ReadBoolean("E 1.0")
                        Thread.Sleep(TimeSpan.FromMilliseconds(100))
                    End While
                Next
            End If

            ' 2. Way: Bulk write (with variables).
            If True Then
                ' Or use the higher level methods of the PLC device connection to write a whole
                ' set of variables at once. While this way would be much faster than the
                ' previous one, because the values are write within one transaction instead of
                ' processing each value within a transaction for each action.

                Dim numberOfPages As New PlcByte("DB111.DBB 2")
                Dim resolution As New PlcInt16("DB111.DBW 4")
                Dim lineHeight As New PlcInt32("DB111.DBD 6")
                Dim price As New PlcReal("DB111.DBD 10")
                Dim articleNumber As New PlcString("DB111.DBB 20", 16)

                Dim startPrint As New PlcBoolean("DB111.DBX 1.0", True)

                For Each line As String In File.ReadAllLines(".\Data.csv")
                    Dim values As String() = line.Split(";"c)

                    ' Number of pages.
                    numberOfPages.Value = Convert.ToByte(values(0))

                    ' Resolution in dpi.
                    resolution.Value = Convert.ToInt16(values(1))

                    ' Line Height in pixels.
                    lineHeight.Value = Convert.ToInt32(values(2))

                    ' Price.
                    price.Value = Convert.ToSingle(values(3))

                    ' Article Number.
                    articleNumber.Value = Convert.ToString(values(4))

                    connection.WriteValues(numberOfPages, resolution, lineHeight, price, articleNumber, startPrint)

                    ' Wait while printing.
                    While connection.ReadBoolean("E 1.0")
                        Thread.Sleep(TimeSpan.FromMilliseconds(100))
                    End While
                Next
            End If

            ' 3. Way: Bulk write (with object).
            If True Then
                ' Or use the methods of the PLC device connection at the highest abstraction
                ' layer to write the whole PLC data at once from a user defined PLC object.

                For Each line As String In File.ReadAllLines(".\Data.csv")
                    Dim values As String() = line.Split(";"c)
                    Dim data As New PrintJobData()

                    ' Number of pages.
                    data.NumberOfPages = Convert.ToByte(values(0))

                    ' Resolution in dpi.
                    data.Resolution = Convert.ToInt16(values(1))

                    ' Line Height in pixels.
                    data.LineHeight = Convert.ToInt32(values(2))

                    ' Price.
                    data.Price = Convert.ToSingle(values(3))

                    ' Article Number.
                    data.ArticleNumber = Convert.ToString(values(4))

                    connection.Write(data)

                    ' Wait while printing.
                    While connection.ReadBoolean("E 1.0")
                        Thread.Sleep(TimeSpan.FromMilliseconds(100))
                    End While
                Next
            End If

            connection.Close()
        End Sub
    End Class
End Namespace

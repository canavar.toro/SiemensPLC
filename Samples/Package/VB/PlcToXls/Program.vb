﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports System
Imports System.Data.OleDb
Imports System.Threading

Imports IPS7Lnk.Advanced

Namespace PlcToXls
    ''' <summary>
    ''' This sample demonstrates a weather station logger application which writes PLC weather
    ''' data to a XLS file.
    ''' </summary>
    ''' <remarks>
    ''' This application does read the weather data from the PLC and when writes the data to a XLS
    ''' file. After a weather record has been written by the logger, the logger does wait for 30
    ''' minutes before the next record will be read from the PLC.
    ''' </remarks>
    Public Class Program
        Public Shared Sub Main(args As String())
            Dim device As New SiemensDevice( _
                    New IPDeviceEndPoint("192.168.0.80"), SiemensDeviceType.S7300_400)

            Dim connection As PlcDeviceConnection = device.CreateConnection()
            connection.Open()

            Dim excelConnection As New OleDbConnection( _
                    "Provider=Microsoft.Jet.OLEDB.4.0;" _
                    & "Data Source=.\Data.xls;" _
                    & "Extended Properties=Excel 8.0")

            excelConnection.Open()

            ' 'Data' represents the Excel Worksheet to write.
            Dim command As OleDbCommand = excelConnection.CreateCommand()
            command.CommandText = "insert into [Data$] values (?, ?, ?, ?, ?)"

            Dim excelChanceOfRain As OleDbParameter = command.Parameters.Add("@chanceOfRain", OleDbType.UnsignedTinyInt)
            Dim excelWindSpeed As OleDbParameter = command.Parameters.Add("@windSpeed", OleDbType.SmallInt)
            Dim excelPressure As OleDbParameter = command.Parameters.Add("@pressure", OleDbType.[Integer])
            Dim excelTemperature As OleDbParameter = command.Parameters.Add("@temperature", OleDbType.[Single])
            Dim excelForecast As OleDbParameter = command.Parameters.Add("@forecast", OleDbType.BSTR)

            ' 1. Way: Sequential Read.
            If True Then
                ' Either use the primitive low level methods of the PLC device connection to
                ' sequential read the desired data areas.

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    ' Chance of rain.
                    excelChanceOfRain.Value = connection.ReadByte("DB111.DBB 2")

                    ' Wind speed.
                    excelWindSpeed.Value = connection.ReadInt16("DB111.DBW 4")

                    ' Pressure.
                    excelPressure.Value = connection.ReadInt32("DB111.DBD 6")

                    ' Temperature.
                    excelTemperature.Value = connection.ReadReal("DB111.DBD 10")

                    ' Forecast.
                    excelForecast.Value = connection.ReadString("DB111.DBB 20", 16)

                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            ' 2. Way: Bulk read (with variables).
            If True Then
                ' Or use the higher level methods of the PLC device connection to read a whole
                ' set of variables at once. While this way would be much faster than the previous
                ' one, because the values are read within one transaction instead of querying
                ' each value within a transaction for each request.

                Dim chanceOfRain As New PlcByte("DB111.DBB 2")
                Dim windSpeed As New PlcInt16("DB111.DBW 4")
                Dim pressure As New PlcInt32("DB111.DBD 6")
                Dim temperature As New PlcReal("DB111.DBD 10")
                Dim forecast As New PlcString("DB111.DBB 20", 16)

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    connection.ReadValues(chanceOfRain, windSpeed, pressure, temperature, forecast)

                    ' Chance of rain.
                    excelChanceOfRain.Value = chanceOfRain.Value

                    ' Wind speed.
                    excelWindSpeed.Value = windSpeed.Value

                    ' Pressure.
                    excelPressure.Value = pressure.Value

                    ' Temperature.
                    excelTemperature.Value = temperature.Value

                    ' Forecast.
                    excelForecast.Value = forecast.Value

                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            ' 3. Way: Bulk read (with object).
            If True Then
                ' Or use the methods of the PLC device connection at the highest abstraction
                ' layer to read the whole PLC data at once into a user defined PLC object.

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    Dim data As WeatherData = connection.ReadObject(Of WeatherData)()

                    ' Chance of rain.
                    excelChanceOfRain.Value = data.ChanceOfRain

                    ' Wind speed.
                    excelWindSpeed.Value = data.WindSpeed

                    ' Pressure.
                    excelPressure.Value = data.Pressure

                    ' Temperature.
                    excelTemperature.Value = data.Temperature

                    ' Forecast.
                    excelForecast.Value = data.Forecast

                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            excelConnection.Close()
            connection.Close()
        End Sub
    End Class
End Namespace


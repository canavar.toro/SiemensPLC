﻿' Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

Imports System
Imports System.Data
Imports System.Data.SqlServerCe
Imports System.Threading

Imports IPS7Lnk.Advanced

Namespace PlcToSql
    ''' <summary>
    ''' This sample demonstrates a weather station logger application which writes PLC weather
    ''' data to a Microsoft SQL Server database.
    ''' </summary>
    ''' <remarks>
    ''' This application does read the weather data from the PLC and when writes the data to a
    ''' Microsoft SQL Server database. After a weather record has been written by the logger,
    ''' the logger does wait for 30 minutes before the next record will be read from the PLC.
    ''' </remarks>
    Public Class Program
        Public Shared Sub Main(args As String())
            Dim device As New SiemensDevice( _
                    New IPDeviceEndPoint("192.168.0.80"), SiemensDeviceType.S7300_400)

            Dim connection As PlcDeviceConnection = device.CreateConnection()
            connection.Open()

            Dim sqlConnection As New SqlCeConnection("Data Source=.\Database.sdf")
            sqlConnection.Open()

            Dim command As SqlCeCommand = sqlConnection.CreateCommand()
            command.CommandText _
                    = "insert into Data (ChanceOfRain, WindSpeed, Pressure, Temperature, Forecast) " _
                    & "values (@chanceOfRain, @windSpeed, @pressure, @temperature, @forecast)"

            Dim sqlChanceOfRain As SqlParameter = command.Parameters.Add("@chanceOfRain", SqlDbType.TinyInt)
            Dim sqlWindSpeed As SqlCeParameter = command.Parameters.Add("@windSpeed", SqlDbType.SmallInt)
            Dim sqlPressure As SqlCeParameter = command.Parameters.Add("@pressure", SqlDbType.Int)
            Dim sqlTemperature As SqlCeParameter = command.Parameters.Add("@temperature", SqlDbType.Real)
            Dim sqlForecast As SqlCeParameter = command.Parameters.Add("@forecast", SqlDbType.NText)

            ' 1. Way: Sequential Read.
            If True Then
                ' Either use the primitive low level methods of the PLC device connection to
                ' sequential read the desired data areas.

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    sqlChanceOfRain.Value = connection.ReadByte("DB111.DBB 2")
                    ' Chance of rain.
                    sqlWindSpeed.Value = connection.ReadInt16("DB111.DBW 4")
                    ' Wind speed.
                    sqlPressure.Value = connection.ReadInt32("DB111.DBD 6")
                    ' Pressure.
                    sqlTemperature.Value = connection.ReadReal("DB111.DBD 10")
                    ' Temperature.
                    sqlForecast.Value = connection.ReadString("DB111.DBB 20", 16)
                    ' Forecast.
                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            ' 2. Way: Bulk read (with variables).
            If True Then
                ' Or use the higher level methods of the PLC device connection to read a whole
                ' set of variables at once. While this way would be much faster than the previous
                ' one, because the values are read within one transaction instead of querying
                ' each value within a transaction for each request.

                Dim chanceOfRain As New PlcByte("DB111.DBB 2")
                Dim windSpeed As New PlcInt16("DB111.DBW 4")
                Dim pressure As New PlcInt32("DB111.DBD 6")
                Dim temperature As New PlcReal("DB111.DBD 10")
                Dim forecast As New PlcString("DB111.DBB 20", 16)

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    connection.ReadValues(chanceOfRain, windSpeed, pressure, temperature, forecast)

                    ' Chance of rain.
                    sqlChanceOfRain.Value = chanceOfRain.Value

                    ' Wind speed.
                    sqlWindSpeed.Value = windSpeed.Value

                    ' Pressure.
                    sqlPressure.Value = pressure.Value

                    ' Temperature.
                    sqlTemperature.Value = temperature.Value

                    ' Forecast.
                    sqlForecast.Value = forecast.Value

                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            ' 3. Way: Bulk read (with object).
            If True Then
                ' Or use the methods of the PLC device connection at the highest abstraction
                ' layer to read the whole PLC data at once into a user defined PLC object.

                ' Is the weather station online.
                While connection.ReadBoolean("E 1.0")
                    Dim data As WeatherData = connection.ReadObject(Of WeatherData)()

                    ' Chance of rain.
                    sqlChanceOfRain.Value = data.ChanceOfRain

                    ' Wind speed.
                    sqlWindSpeed.Value = data.WindSpeed

                    ' Pressure.
                    sqlPressure.Value = data.Pressure

                    ' Temperature.
                    sqlTemperature.Value = data.Temperature

                    ' Forecast.
                    sqlForecast.Value = data.Forecast

                    command.ExecuteNonQuery()
                    Thread.Sleep(TimeSpan.FromMinutes(30))
                End While
            End If

            sqlConnection.Close()
            connection.Close()
        End Sub
    End Class
End Namespace


﻿namespace Interactive
{
    partial class DeviceInfoView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.deviceInfoTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.devicePlantIdTextBox = new System.Windows.Forms.TextBox();
            this.deviceModuleSerialTextBox = new System.Windows.Forms.TextBox();
            this.deviceModuleTypeTextBox = new System.Windows.Forms.TextBox();
            this.deviceModuleNameTextBox = new System.Windows.Forms.TextBox();
            this.deviceLocationTextBox = new System.Windows.Forms.TextBox();
            this.deviceLocationLabel = new System.Windows.Forms.Label();
            this.deviceNameLabel = new System.Windows.Forms.Label();
            this.deviceNameTextBox = new System.Windows.Forms.TextBox();
            this.deviceModuleNameLabel = new System.Windows.Forms.Label();
            this.deviceModuleTypeLabel = new System.Windows.Forms.Label();
            this.deviceModuleSerialLabel = new System.Windows.Forms.Label();
            this.devicePlantIdLabel = new System.Windows.Forms.Label();
            this.deviceTimeLabel = new System.Windows.Forms.Label();
            this.deviceTimeTextBox = new System.Windows.Forms.TextBox();
            this.deviceCopyrightTextBox = new System.Windows.Forms.TextBox();
            this.deviceInfoTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // deviceInfoTableLayoutPanel
            // 
            this.deviceInfoTableLayoutPanel.ColumnCount = 2;
            this.deviceInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.deviceInfoTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.deviceInfoTableLayoutPanel.Controls.Add(this.devicePlantIdTextBox, 1, 8);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleSerialTextBox, 1, 6);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleTypeTextBox, 1, 5);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleNameTextBox, 1, 4);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceLocationTextBox, 1, 2);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceLocationLabel, 0, 2);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceNameLabel, 0, 1);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceNameTextBox, 1, 1);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleNameLabel, 0, 4);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleTypeLabel, 0, 5);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceModuleSerialLabel, 0, 6);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.devicePlantIdLabel, 0, 8);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceTimeLabel, 0, 9);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceTimeTextBox, 1, 9);
            this.deviceInfoTableLayoutPanel.Controls.Add(this.deviceCopyrightTextBox, 1, 10);
            this.deviceInfoTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceInfoTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.deviceInfoTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.deviceInfoTableLayoutPanel.Name = "deviceInfoTableLayoutPanel";
            this.deviceInfoTableLayoutPanel.RowCount = 11;
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.deviceInfoTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.deviceInfoTableLayoutPanel.Size = new System.Drawing.Size(334, 216);
            this.deviceInfoTableLayoutPanel.TabIndex = 1;
            // 
            // devicePlantIdTextBox
            // 
            this.devicePlantIdTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.devicePlantIdTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.devicePlantIdTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.devicePlantIdTextBox.Location = new System.Drawing.Point(123, 134);
            this.devicePlantIdTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.devicePlantIdTextBox.Name = "devicePlantIdTextBox";
            this.devicePlantIdTextBox.ReadOnly = true;
            this.devicePlantIdTextBox.Size = new System.Drawing.Size(208, 13);
            this.devicePlantIdTextBox.TabIndex = 12;
            // 
            // deviceModuleSerialTextBox
            // 
            this.deviceModuleSerialTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceModuleSerialTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceModuleSerialTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceModuleSerialTextBox.Location = new System.Drawing.Point(123, 104);
            this.deviceModuleSerialTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceModuleSerialTextBox.Name = "deviceModuleSerialTextBox";
            this.deviceModuleSerialTextBox.ReadOnly = true;
            this.deviceModuleSerialTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceModuleSerialTextBox.TabIndex = 10;
            // 
            // deviceModuleTypeTextBox
            // 
            this.deviceModuleTypeTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceModuleTypeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceModuleTypeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceModuleTypeTextBox.Location = new System.Drawing.Point(123, 84);
            this.deviceModuleTypeTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceModuleTypeTextBox.Name = "deviceModuleTypeTextBox";
            this.deviceModuleTypeTextBox.ReadOnly = true;
            this.deviceModuleTypeTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceModuleTypeTextBox.TabIndex = 8;
            // 
            // deviceModuleNameTextBox
            // 
            this.deviceModuleNameTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceModuleNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceModuleNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceModuleNameTextBox.Location = new System.Drawing.Point(123, 64);
            this.deviceModuleNameTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceModuleNameTextBox.Name = "deviceModuleNameTextBox";
            this.deviceModuleNameTextBox.ReadOnly = true;
            this.deviceModuleNameTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceModuleNameTextBox.TabIndex = 6;
            // 
            // deviceLocationTextBox
            // 
            this.deviceLocationTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceLocationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceLocationTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceLocationTextBox.Location = new System.Drawing.Point(123, 34);
            this.deviceLocationTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceLocationTextBox.Name = "deviceLocationTextBox";
            this.deviceLocationTextBox.ReadOnly = true;
            this.deviceLocationTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceLocationTextBox.TabIndex = 4;
            // 
            // deviceLocationLabel
            // 
            this.deviceLocationLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceLocationLabel.AutoSize = true;
            this.deviceLocationLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceLocationLabel.Location = new System.Drawing.Point(66, 33);
            this.deviceLocationLabel.Name = "deviceLocationLabel";
            this.deviceLocationLabel.Size = new System.Drawing.Size(51, 13);
            this.deviceLocationLabel.TabIndex = 3;
            this.deviceLocationLabel.Text = "Location:";
            // 
            // deviceNameLabel
            // 
            this.deviceNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceNameLabel.AutoSize = true;
            this.deviceNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceNameLabel.Location = new System.Drawing.Point(79, 13);
            this.deviceNameLabel.Name = "deviceNameLabel";
            this.deviceNameLabel.Size = new System.Drawing.Size(38, 13);
            this.deviceNameLabel.TabIndex = 1;
            this.deviceNameLabel.Text = "Name:";
            // 
            // deviceNameTextBox
            // 
            this.deviceNameTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceNameTextBox.Location = new System.Drawing.Point(123, 14);
            this.deviceNameTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceNameTextBox.Name = "deviceNameTextBox";
            this.deviceNameTextBox.ReadOnly = true;
            this.deviceNameTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceNameTextBox.TabIndex = 2;
            // 
            // deviceModuleNameLabel
            // 
            this.deviceModuleNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceModuleNameLabel.AutoSize = true;
            this.deviceModuleNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceModuleNameLabel.Location = new System.Drawing.Point(41, 63);
            this.deviceModuleNameLabel.Name = "deviceModuleNameLabel";
            this.deviceModuleNameLabel.Size = new System.Drawing.Size(76, 13);
            this.deviceModuleNameLabel.TabIndex = 5;
            this.deviceModuleNameLabel.Text = "Module Name:";
            // 
            // deviceModuleTypeLabel
            // 
            this.deviceModuleTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceModuleTypeLabel.AutoSize = true;
            this.deviceModuleTypeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceModuleTypeLabel.Location = new System.Drawing.Point(45, 83);
            this.deviceModuleTypeLabel.Name = "deviceModuleTypeLabel";
            this.deviceModuleTypeLabel.Size = new System.Drawing.Size(72, 13);
            this.deviceModuleTypeLabel.TabIndex = 7;
            this.deviceModuleTypeLabel.Text = "Module Type:";
            // 
            // deviceModuleSerialLabel
            // 
            this.deviceModuleSerialLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceModuleSerialLabel.AutoSize = true;
            this.deviceModuleSerialLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceModuleSerialLabel.Location = new System.Drawing.Point(43, 103);
            this.deviceModuleSerialLabel.Name = "deviceModuleSerialLabel";
            this.deviceModuleSerialLabel.Size = new System.Drawing.Size(74, 13);
            this.deviceModuleSerialLabel.TabIndex = 9;
            this.deviceModuleSerialLabel.Text = "Module Serial:";
            // 
            // devicePlantIdLabel
            // 
            this.devicePlantIdLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.devicePlantIdLabel.AutoSize = true;
            this.devicePlantIdLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.devicePlantIdLabel.Location = new System.Drawing.Point(69, 133);
            this.devicePlantIdLabel.Name = "devicePlantIdLabel";
            this.devicePlantIdLabel.Size = new System.Drawing.Size(48, 13);
            this.devicePlantIdLabel.TabIndex = 11;
            this.devicePlantIdLabel.Text = "Plant ID:";
            // 
            // deviceTimeLabel
            // 
            this.deviceTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.deviceTimeLabel.AutoSize = true;
            this.deviceTimeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.deviceTimeLabel.Location = new System.Drawing.Point(84, 153);
            this.deviceTimeLabel.Name = "deviceTimeLabel";
            this.deviceTimeLabel.Size = new System.Drawing.Size(33, 13);
            this.deviceTimeLabel.TabIndex = 15;
            this.deviceTimeLabel.Text = "Time:";
            // 
            // deviceTimeTextBox
            // 
            this.deviceTimeTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceTimeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceTimeTextBox.Location = new System.Drawing.Point(123, 154);
            this.deviceTimeTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 3);
            this.deviceTimeTextBox.Name = "deviceTimeTextBox";
            this.deviceTimeTextBox.ReadOnly = true;
            this.deviceTimeTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceTimeTextBox.TabIndex = 14;
            // 
            // deviceCopyrightTextBox
            // 
            this.deviceCopyrightTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.deviceCopyrightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceCopyrightTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deviceCopyrightTextBox.ForeColor = System.Drawing.Color.Silver;
            this.deviceCopyrightTextBox.Location = new System.Drawing.Point(123, 173);
            this.deviceCopyrightTextBox.Name = "deviceCopyrightTextBox";
            this.deviceCopyrightTextBox.ReadOnly = true;
            this.deviceCopyrightTextBox.Size = new System.Drawing.Size(208, 13);
            this.deviceCopyrightTextBox.TabIndex = 13;
            // 
            // DeviceInfoView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 216);
            this.Controls.Add(this.deviceInfoTableLayoutPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DeviceInfoView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Device Info";
            this.deviceInfoTableLayoutPanel.ResumeLayout(false);
            this.deviceInfoTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel deviceInfoTableLayoutPanel;
        private System.Windows.Forms.TextBox devicePlantIdTextBox;
        private System.Windows.Forms.TextBox deviceModuleSerialTextBox;
        private System.Windows.Forms.TextBox deviceModuleTypeTextBox;
        private System.Windows.Forms.TextBox deviceModuleNameTextBox;
        private System.Windows.Forms.TextBox deviceLocationTextBox;
        private System.Windows.Forms.Label deviceLocationLabel;
        private System.Windows.Forms.Label deviceNameLabel;
        private System.Windows.Forms.TextBox deviceNameTextBox;
        private System.Windows.Forms.Label deviceModuleNameLabel;
        private System.Windows.Forms.Label deviceModuleTypeLabel;
        private System.Windows.Forms.Label deviceModuleSerialLabel;
        private System.Windows.Forms.Label devicePlantIdLabel;
        private System.Windows.Forms.Label deviceTimeLabel;
        private System.Windows.Forms.TextBox deviceTimeTextBox;
        private System.Windows.Forms.TextBox deviceCopyrightTextBox;
    }
}
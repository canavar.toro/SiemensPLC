﻿namespace Interactive
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.isMdiContainerDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.keyPreviewDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.opacityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rightToLeftLayoutDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.topMostDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.startPositionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transparencyKeyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showInTaskbarDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sizeGripStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showIconDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iconDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.sizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.helpButtonDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.maximumSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acceptButtonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minimizeBoxDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.StatusTimer = new System.Windows.Forms.Timer(this.components);
            this.minimumSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maximizeBoxDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.formBorderStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mainMenuStripDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancelButtonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.allowDropDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.backgroundImageLayoutDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autoScrollMinSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accessibleRoleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accessibleDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.causesValidationDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.accessibleNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.foreColorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rightToLeftDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tagDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cursorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enabledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataBindingsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.backgroundImageDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.useWaitCursorDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.backColorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.controlBoxDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.autoScrollDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.autoValidateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autoSizeDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.windowStateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autoSizeModeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anchorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.autoScrollMarginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imeModeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paddingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.visibleDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.channelTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.channelAddressLabel = new System.Windows.Forms.Label();
            this.channelAddressTextBox = new System.Windows.Forms.TextBox();
            this.channelRackLabel = new System.Windows.Forms.Label();
            this.channelSlotLabel = new System.Windows.Forms.Label();
            this.channelSlotNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.channelRackNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.channelDeviceTypeLabel = new System.Windows.Forms.Label();
            this.channelDeviceTypeComboBox = new System.Windows.Forms.ComboBox();
            this.channelChannelTypeLabel = new System.Windows.Forms.Label();
            this.timeoutBreakDetectionCheckBox = new System.Windows.Forms.CheckBox();
            this.testConfigurationButton = new System.Windows.Forms.Button();
            this.timeoutBreakDetectionPanel = new System.Windows.Forms.Panel();
            this.timeoutBreakDetectionMillisecondsLabel = new System.Windows.Forms.Label();
            this.timeoutBreakDetectionNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.channelChannelTypeComboBox = new System.Windows.Forms.ComboBox();
            this.channelGroupBox = new System.Windows.Forms.GroupBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.openConnectToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.showAboutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.showHelpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.readDataGroupBox = new System.Windows.Forms.GroupBox();
            this.addressTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.readAddressLabel = new System.Windows.Forms.Label();
            this.readAddressDataAreaComboBox = new System.Windows.Forms.ComboBox();
            this.readAddressDataAreaLabel = new System.Windows.Forms.Label();
            this.readAddressDataBlockNumberLabel = new System.Windows.Forms.Label();
            this.readAddressDataBlockNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.readAddressTextBox = new System.Windows.Forms.TextBox();
            this.readAddressByteNumberLabel = new System.Windows.Forms.Label();
            this.readAddressByteNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.readAddressBitNumberLabel = new System.Windows.Forms.Label();
            this.readAddressBitNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.readAddressRawTypeNumberLabel = new System.Windows.Forms.Label();
            this.readAddressRawTypeComboBox = new System.Windows.Forms.ComboBox();
            this.readCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.readCountLabel = new System.Windows.Forms.Label();
            this.readTypeLabel = new System.Windows.Forms.Label();
            this.readTypeComboBox = new System.Windows.Forms.ComboBox();
            this.readButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.channelTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channelSlotNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelRackNumericUpDown)).BeginInit();
            this.timeoutBreakDetectionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeoutBreakDetectionNumericUpDown)).BeginInit();
            this.channelGroupBox.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.readDataGroupBox.SuspendLayout();
            this.addressTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressDataBlockNumberNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressByteNumberNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressBitNumberNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // isMdiContainerDataGridViewCheckBoxColumn
            // 
            this.isMdiContainerDataGridViewCheckBoxColumn.DataPropertyName = "IsMdiContainer";
            this.isMdiContainerDataGridViewCheckBoxColumn.HeaderText = "IsMdiContainer";
            this.isMdiContainerDataGridViewCheckBoxColumn.Name = "isMdiContainerDataGridViewCheckBoxColumn";
            // 
            // keyPreviewDataGridViewCheckBoxColumn
            // 
            this.keyPreviewDataGridViewCheckBoxColumn.DataPropertyName = "KeyPreview";
            this.keyPreviewDataGridViewCheckBoxColumn.HeaderText = "KeyPreview";
            this.keyPreviewDataGridViewCheckBoxColumn.Name = "keyPreviewDataGridViewCheckBoxColumn";
            // 
            // opacityDataGridViewTextBoxColumn
            // 
            this.opacityDataGridViewTextBoxColumn.DataPropertyName = "Opacity";
            this.opacityDataGridViewTextBoxColumn.HeaderText = "Opacity";
            this.opacityDataGridViewTextBoxColumn.Name = "opacityDataGridViewTextBoxColumn";
            // 
            // locationDataGridViewTextBoxColumn
            // 
            this.locationDataGridViewTextBoxColumn.DataPropertyName = "Location";
            this.locationDataGridViewTextBoxColumn.HeaderText = "Location";
            this.locationDataGridViewTextBoxColumn.Name = "locationDataGridViewTextBoxColumn";
            // 
            // textDataGridViewTextBoxColumn
            // 
            this.textDataGridViewTextBoxColumn.DataPropertyName = "Text";
            this.textDataGridViewTextBoxColumn.HeaderText = "Text";
            this.textDataGridViewTextBoxColumn.Name = "textDataGridViewTextBoxColumn";
            // 
            // rightToLeftLayoutDataGridViewCheckBoxColumn
            // 
            this.rightToLeftLayoutDataGridViewCheckBoxColumn.DataPropertyName = "RightToLeftLayout";
            this.rightToLeftLayoutDataGridViewCheckBoxColumn.HeaderText = "RightToLeftLayout";
            this.rightToLeftLayoutDataGridViewCheckBoxColumn.Name = "rightToLeftLayoutDataGridViewCheckBoxColumn";
            // 
            // topMostDataGridViewCheckBoxColumn
            // 
            this.topMostDataGridViewCheckBoxColumn.DataPropertyName = "TopMost";
            this.topMostDataGridViewCheckBoxColumn.HeaderText = "TopMost";
            this.topMostDataGridViewCheckBoxColumn.Name = "topMostDataGridViewCheckBoxColumn";
            // 
            // startPositionDataGridViewTextBoxColumn
            // 
            this.startPositionDataGridViewTextBoxColumn.DataPropertyName = "StartPosition";
            this.startPositionDataGridViewTextBoxColumn.HeaderText = "StartPosition";
            this.startPositionDataGridViewTextBoxColumn.MaxInputLength = 100;
            this.startPositionDataGridViewTextBoxColumn.Name = "startPositionDataGridViewTextBoxColumn";
            // 
            // transparencyKeyDataGridViewTextBoxColumn
            // 
            this.transparencyKeyDataGridViewTextBoxColumn.DataPropertyName = "TransparencyKey";
            this.transparencyKeyDataGridViewTextBoxColumn.HeaderText = "TransparencyKey";
            this.transparencyKeyDataGridViewTextBoxColumn.Name = "transparencyKeyDataGridViewTextBoxColumn";
            // 
            // showInTaskbarDataGridViewCheckBoxColumn
            // 
            this.showInTaskbarDataGridViewCheckBoxColumn.DataPropertyName = "ShowInTaskbar";
            this.showInTaskbarDataGridViewCheckBoxColumn.HeaderText = "ShowInTaskbar";
            this.showInTaskbarDataGridViewCheckBoxColumn.Name = "showInTaskbarDataGridViewCheckBoxColumn";
            // 
            // sizeGripStyleDataGridViewTextBoxColumn
            // 
            this.sizeGripStyleDataGridViewTextBoxColumn.DataPropertyName = "SizeGripStyle";
            this.sizeGripStyleDataGridViewTextBoxColumn.HeaderText = "SizeGripStyle";
            this.sizeGripStyleDataGridViewTextBoxColumn.Name = "sizeGripStyleDataGridViewTextBoxColumn";
            // 
            // showIconDataGridViewCheckBoxColumn
            // 
            this.showIconDataGridViewCheckBoxColumn.DataPropertyName = "ShowIcon";
            this.showIconDataGridViewCheckBoxColumn.HeaderText = "ShowIcon";
            this.showIconDataGridViewCheckBoxColumn.Name = "showIconDataGridViewCheckBoxColumn";
            // 
            // iconDataGridViewImageColumn
            // 
            this.iconDataGridViewImageColumn.DataPropertyName = "Icon";
            this.iconDataGridViewImageColumn.HeaderText = "Icon";
            this.iconDataGridViewImageColumn.Name = "iconDataGridViewImageColumn";
            // 
            // sizeDataGridViewTextBoxColumn
            // 
            this.sizeDataGridViewTextBoxColumn.DataPropertyName = "Size";
            this.sizeDataGridViewTextBoxColumn.HeaderText = "Size";
            this.sizeDataGridViewTextBoxColumn.Name = "sizeDataGridViewTextBoxColumn";
            // 
            // helpButtonDataGridViewCheckBoxColumn
            // 
            this.helpButtonDataGridViewCheckBoxColumn.DataPropertyName = "HelpButton";
            this.helpButtonDataGridViewCheckBoxColumn.HeaderText = "HelpButton";
            this.helpButtonDataGridViewCheckBoxColumn.Name = "helpButtonDataGridViewCheckBoxColumn";
            // 
            // maximumSizeDataGridViewTextBoxColumn
            // 
            this.maximumSizeDataGridViewTextBoxColumn.DataPropertyName = "MaximumSize";
            this.maximumSizeDataGridViewTextBoxColumn.HeaderText = "MaximumSize";
            this.maximumSizeDataGridViewTextBoxColumn.Name = "maximumSizeDataGridViewTextBoxColumn";
            // 
            // acceptButtonDataGridViewTextBoxColumn
            // 
            this.acceptButtonDataGridViewTextBoxColumn.DataPropertyName = "AcceptButton";
            this.acceptButtonDataGridViewTextBoxColumn.HeaderText = "AcceptButton";
            this.acceptButtonDataGridViewTextBoxColumn.Name = "acceptButtonDataGridViewTextBoxColumn";
            // 
            // minimizeBoxDataGridViewCheckBoxColumn
            // 
            this.minimizeBoxDataGridViewCheckBoxColumn.DataPropertyName = "MinimizeBox";
            this.minimizeBoxDataGridViewCheckBoxColumn.HeaderText = "MinimizeBox";
            this.minimizeBoxDataGridViewCheckBoxColumn.Name = "minimizeBoxDataGridViewCheckBoxColumn";
            // 
            // StatusTimer
            // 
            this.StatusTimer.Interval = 2000;
            // 
            // minimumSizeDataGridViewTextBoxColumn
            // 
            this.minimumSizeDataGridViewTextBoxColumn.DataPropertyName = "MinimumSize";
            this.minimumSizeDataGridViewTextBoxColumn.HeaderText = "MinimumSize";
            this.minimumSizeDataGridViewTextBoxColumn.Name = "minimumSizeDataGridViewTextBoxColumn";
            // 
            // maximizeBoxDataGridViewCheckBoxColumn
            // 
            this.maximizeBoxDataGridViewCheckBoxColumn.DataPropertyName = "MaximizeBox";
            this.maximizeBoxDataGridViewCheckBoxColumn.HeaderText = "MaximizeBox";
            this.maximizeBoxDataGridViewCheckBoxColumn.Name = "maximizeBoxDataGridViewCheckBoxColumn";
            // 
            // formBorderStyleDataGridViewTextBoxColumn
            // 
            this.formBorderStyleDataGridViewTextBoxColumn.DataPropertyName = "FormBorderStyle";
            this.formBorderStyleDataGridViewTextBoxColumn.HeaderText = "FormBorderStyle";
            this.formBorderStyleDataGridViewTextBoxColumn.Name = "formBorderStyleDataGridViewTextBoxColumn";
            // 
            // mainMenuStripDataGridViewTextBoxColumn
            // 
            this.mainMenuStripDataGridViewTextBoxColumn.DataPropertyName = "MainMenuStrip";
            this.mainMenuStripDataGridViewTextBoxColumn.HeaderText = "MainMenuStrip";
            this.mainMenuStripDataGridViewTextBoxColumn.Name = "mainMenuStripDataGridViewTextBoxColumn";
            // 
            // cancelButtonDataGridViewTextBoxColumn
            // 
            this.cancelButtonDataGridViewTextBoxColumn.DataPropertyName = "CancelButton";
            this.cancelButtonDataGridViewTextBoxColumn.HeaderText = "CancelButton";
            this.cancelButtonDataGridViewTextBoxColumn.Name = "cancelButtonDataGridViewTextBoxColumn";
            // 
            // allowDropDataGridViewCheckBoxColumn
            // 
            this.allowDropDataGridViewCheckBoxColumn.DataPropertyName = "AllowDrop";
            this.allowDropDataGridViewCheckBoxColumn.HeaderText = "AllowDrop";
            this.allowDropDataGridViewCheckBoxColumn.Name = "allowDropDataGridViewCheckBoxColumn";
            // 
            // backgroundImageLayoutDataGridViewTextBoxColumn
            // 
            this.backgroundImageLayoutDataGridViewTextBoxColumn.DataPropertyName = "BackgroundImageLayout";
            this.backgroundImageLayoutDataGridViewTextBoxColumn.HeaderText = "BackgroundImageLayout";
            this.backgroundImageLayoutDataGridViewTextBoxColumn.Name = "backgroundImageLayoutDataGridViewTextBoxColumn";
            // 
            // autoScrollMinSizeDataGridViewTextBoxColumn
            // 
            this.autoScrollMinSizeDataGridViewTextBoxColumn.DataPropertyName = "AutoScrollMinSize";
            this.autoScrollMinSizeDataGridViewTextBoxColumn.HeaderText = "AutoScrollMinSize";
            this.autoScrollMinSizeDataGridViewTextBoxColumn.Name = "autoScrollMinSizeDataGridViewTextBoxColumn";
            // 
            // accessibleRoleDataGridViewTextBoxColumn
            // 
            this.accessibleRoleDataGridViewTextBoxColumn.DataPropertyName = "AccessibleRole";
            this.accessibleRoleDataGridViewTextBoxColumn.HeaderText = "AccessibleRole";
            this.accessibleRoleDataGridViewTextBoxColumn.Name = "accessibleRoleDataGridViewTextBoxColumn";
            // 
            // accessibleDescriptionDataGridViewTextBoxColumn
            // 
            this.accessibleDescriptionDataGridViewTextBoxColumn.DataPropertyName = "AccessibleDescription";
            this.accessibleDescriptionDataGridViewTextBoxColumn.HeaderText = "AccessibleDescription";
            this.accessibleDescriptionDataGridViewTextBoxColumn.Name = "accessibleDescriptionDataGridViewTextBoxColumn";
            // 
            // causesValidationDataGridViewCheckBoxColumn
            // 
            this.causesValidationDataGridViewCheckBoxColumn.DataPropertyName = "CausesValidation";
            this.causesValidationDataGridViewCheckBoxColumn.HeaderText = "CausesValidation";
            this.causesValidationDataGridViewCheckBoxColumn.Name = "causesValidationDataGridViewCheckBoxColumn";
            // 
            // accessibleNameDataGridViewTextBoxColumn
            // 
            this.accessibleNameDataGridViewTextBoxColumn.DataPropertyName = "AccessibleName";
            this.accessibleNameDataGridViewTextBoxColumn.HeaderText = "AccessibleName";
            this.accessibleNameDataGridViewTextBoxColumn.Name = "accessibleNameDataGridViewTextBoxColumn";
            // 
            // foreColorDataGridViewTextBoxColumn
            // 
            this.foreColorDataGridViewTextBoxColumn.DataPropertyName = "ForeColor";
            this.foreColorDataGridViewTextBoxColumn.HeaderText = "ForeColor";
            this.foreColorDataGridViewTextBoxColumn.Name = "foreColorDataGridViewTextBoxColumn";
            // 
            // contextMenuStripDataGridViewTextBoxColumn
            // 
            this.contextMenuStripDataGridViewTextBoxColumn.DataPropertyName = "ContextMenuStrip";
            this.contextMenuStripDataGridViewTextBoxColumn.HeaderText = "ContextMenuStrip";
            this.contextMenuStripDataGridViewTextBoxColumn.Name = "contextMenuStripDataGridViewTextBoxColumn";
            // 
            // rightToLeftDataGridViewTextBoxColumn
            // 
            this.rightToLeftDataGridViewTextBoxColumn.DataPropertyName = "RightToLeft";
            this.rightToLeftDataGridViewTextBoxColumn.HeaderText = "RightToLeft";
            this.rightToLeftDataGridViewTextBoxColumn.MaxInputLength = 4;
            this.rightToLeftDataGridViewTextBoxColumn.Name = "rightToLeftDataGridViewTextBoxColumn";
            // 
            // fontDataGridViewTextBoxColumn
            // 
            this.fontDataGridViewTextBoxColumn.DataPropertyName = "Font";
            this.fontDataGridViewTextBoxColumn.HeaderText = "Font";
            this.fontDataGridViewTextBoxColumn.Name = "fontDataGridViewTextBoxColumn";
            // 
            // tagDataGridViewTextBoxColumn
            // 
            this.tagDataGridViewTextBoxColumn.DataPropertyName = "Tag";
            this.tagDataGridViewTextBoxColumn.HeaderText = "Tag";
            this.tagDataGridViewTextBoxColumn.Name = "tagDataGridViewTextBoxColumn";
            // 
            // cursorDataGridViewTextBoxColumn
            // 
            this.cursorDataGridViewTextBoxColumn.DataPropertyName = "Cursor";
            this.cursorDataGridViewTextBoxColumn.HeaderText = "Cursor";
            this.cursorDataGridViewTextBoxColumn.Name = "cursorDataGridViewTextBoxColumn";
            // 
            // enabledDataGridViewCheckBoxColumn
            // 
            this.enabledDataGridViewCheckBoxColumn.DataPropertyName = "Enabled";
            this.enabledDataGridViewCheckBoxColumn.HeaderText = "Enabled";
            this.enabledDataGridViewCheckBoxColumn.Name = "enabledDataGridViewCheckBoxColumn";
            // 
            // dataBindingsDataGridViewTextBoxColumn
            // 
            this.dataBindingsDataGridViewTextBoxColumn.DataPropertyName = "DataBindings";
            this.dataBindingsDataGridViewTextBoxColumn.HeaderText = "DataBindings";
            this.dataBindingsDataGridViewTextBoxColumn.Name = "dataBindingsDataGridViewTextBoxColumn";
            this.dataBindingsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dockDataGridViewTextBoxColumn
            // 
            this.dockDataGridViewTextBoxColumn.DataPropertyName = "Dock";
            this.dockDataGridViewTextBoxColumn.HeaderText = "Dock";
            this.dockDataGridViewTextBoxColumn.Name = "dockDataGridViewTextBoxColumn";
            // 
            // backgroundImageDataGridViewImageColumn
            // 
            this.backgroundImageDataGridViewImageColumn.DataPropertyName = "BackgroundImage";
            this.backgroundImageDataGridViewImageColumn.HeaderText = "BackgroundImage";
            this.backgroundImageDataGridViewImageColumn.Name = "backgroundImageDataGridViewImageColumn";
            // 
            // useWaitCursorDataGridViewCheckBoxColumn
            // 
            this.useWaitCursorDataGridViewCheckBoxColumn.DataPropertyName = "UseWaitCursor";
            this.useWaitCursorDataGridViewCheckBoxColumn.HeaderText = "UseWaitCursor";
            this.useWaitCursorDataGridViewCheckBoxColumn.Name = "useWaitCursorDataGridViewCheckBoxColumn";
            // 
            // backColorDataGridViewTextBoxColumn
            // 
            this.backColorDataGridViewTextBoxColumn.DataPropertyName = "BackColor";
            this.backColorDataGridViewTextBoxColumn.HeaderText = "BackColor";
            this.backColorDataGridViewTextBoxColumn.Name = "backColorDataGridViewTextBoxColumn";
            // 
            // controlBoxDataGridViewCheckBoxColumn
            // 
            this.controlBoxDataGridViewCheckBoxColumn.DataPropertyName = "ControlBox";
            this.controlBoxDataGridViewCheckBoxColumn.HeaderText = "ControlBox";
            this.controlBoxDataGridViewCheckBoxColumn.Name = "controlBoxDataGridViewCheckBoxColumn";
            // 
            // autoScrollDataGridViewCheckBoxColumn
            // 
            this.autoScrollDataGridViewCheckBoxColumn.DataPropertyName = "AutoScroll";
            this.autoScrollDataGridViewCheckBoxColumn.HeaderText = "AutoScroll";
            this.autoScrollDataGridViewCheckBoxColumn.Name = "autoScrollDataGridViewCheckBoxColumn";
            // 
            // autoValidateDataGridViewTextBoxColumn
            // 
            this.autoValidateDataGridViewTextBoxColumn.DataPropertyName = "AutoValidate";
            this.autoValidateDataGridViewTextBoxColumn.HeaderText = "AutoValidate";
            this.autoValidateDataGridViewTextBoxColumn.Name = "autoValidateDataGridViewTextBoxColumn";
            // 
            // autoSizeDataGridViewCheckBoxColumn
            // 
            this.autoSizeDataGridViewCheckBoxColumn.DataPropertyName = "AutoSize";
            this.autoSizeDataGridViewCheckBoxColumn.HeaderText = "AutoSize";
            this.autoSizeDataGridViewCheckBoxColumn.Name = "autoSizeDataGridViewCheckBoxColumn";
            // 
            // windowStateDataGridViewTextBoxColumn
            // 
            this.windowStateDataGridViewTextBoxColumn.DataPropertyName = "WindowState";
            this.windowStateDataGridViewTextBoxColumn.HeaderText = "WindowState";
            this.windowStateDataGridViewTextBoxColumn.Name = "windowStateDataGridViewTextBoxColumn";
            // 
            // autoSizeModeDataGridViewTextBoxColumn
            // 
            this.autoSizeModeDataGridViewTextBoxColumn.DataPropertyName = "AutoSizeMode";
            this.autoSizeModeDataGridViewTextBoxColumn.HeaderText = "AutoSizeMode";
            this.autoSizeModeDataGridViewTextBoxColumn.Name = "autoSizeModeDataGridViewTextBoxColumn";
            // 
            // anchorDataGridViewTextBoxColumn
            // 
            this.anchorDataGridViewTextBoxColumn.DataPropertyName = "Anchor";
            this.anchorDataGridViewTextBoxColumn.HeaderText = "Anchor";
            this.anchorDataGridViewTextBoxColumn.Name = "anchorDataGridViewTextBoxColumn";
            // 
            // autoScrollMarginDataGridViewTextBoxColumn
            // 
            this.autoScrollMarginDataGridViewTextBoxColumn.DataPropertyName = "AutoScrollMargin";
            this.autoScrollMarginDataGridViewTextBoxColumn.HeaderText = "AutoScrollMargin";
            this.autoScrollMarginDataGridViewTextBoxColumn.Name = "autoScrollMarginDataGridViewTextBoxColumn";
            // 
            // imeModeDataGridViewTextBoxColumn
            // 
            this.imeModeDataGridViewTextBoxColumn.DataPropertyName = "ImeMode";
            this.imeModeDataGridViewTextBoxColumn.HeaderText = "ImeMode";
            this.imeModeDataGridViewTextBoxColumn.Name = "imeModeDataGridViewTextBoxColumn";
            // 
            // paddingDataGridViewTextBoxColumn
            // 
            this.paddingDataGridViewTextBoxColumn.DataPropertyName = "Padding";
            this.paddingDataGridViewTextBoxColumn.HeaderText = "Padding";
            this.paddingDataGridViewTextBoxColumn.Name = "paddingDataGridViewTextBoxColumn";
            // 
            // helpProvider
            // 
            this.helpProvider.HelpNamespace = "ips7lnk.chm";
            // 
            // visibleDataGridViewCheckBoxColumn
            // 
            this.visibleDataGridViewCheckBoxColumn.DataPropertyName = "Visible";
            this.visibleDataGridViewCheckBoxColumn.HeaderText = "Visible";
            this.visibleDataGridViewCheckBoxColumn.Name = "visibleDataGridViewCheckBoxColumn";
            // 
            // channelTableLayoutPanel
            // 
            this.channelTableLayoutPanel.ColumnCount = 6;
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.channelTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.channelTableLayoutPanel.Controls.Add(this.channelAddressLabel, 0, 0);
            this.channelTableLayoutPanel.Controls.Add(this.channelAddressTextBox, 1, 0);
            this.channelTableLayoutPanel.Controls.Add(this.channelRackLabel, 0, 1);
            this.channelTableLayoutPanel.Controls.Add(this.channelSlotLabel, 0, 2);
            this.channelTableLayoutPanel.Controls.Add(this.channelSlotNumericUpDown, 1, 1);
            this.channelTableLayoutPanel.Controls.Add(this.channelRackNumericUpDown, 1, 2);
            this.channelTableLayoutPanel.Controls.Add(this.channelDeviceTypeLabel, 3, 0);
            this.channelTableLayoutPanel.Controls.Add(this.channelDeviceTypeComboBox, 4, 0);
            this.channelTableLayoutPanel.Controls.Add(this.channelChannelTypeLabel, 3, 1);
            this.channelTableLayoutPanel.Controls.Add(this.timeoutBreakDetectionCheckBox, 3, 2);
            this.channelTableLayoutPanel.Controls.Add(this.testConfigurationButton, 4, 4);
            this.channelTableLayoutPanel.Controls.Add(this.timeoutBreakDetectionPanel, 4, 2);
            this.channelTableLayoutPanel.Controls.Add(this.channelChannelTypeComboBox, 4, 1);
            this.channelTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.channelTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.channelTableLayoutPanel.Name = "channelTableLayoutPanel";
            this.channelTableLayoutPanel.RowCount = 6;
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.channelTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.channelTableLayoutPanel.Size = new System.Drawing.Size(542, 120);
            this.channelTableLayoutPanel.TabIndex = 94;
            // 
            // channelAddressLabel
            // 
            this.channelAddressLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.channelAddressLabel.AutoSize = true;
            this.channelAddressLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelAddressLabel.Location = new System.Drawing.Point(36, 7);
            this.channelAddressLabel.Name = "channelAddressLabel";
            this.channelAddressLabel.Size = new System.Drawing.Size(61, 13);
            this.channelAddressLabel.TabIndex = 5;
            this.channelAddressLabel.Text = "IP Address:";
            // 
            // channelAddressTextBox
            // 
            this.channelAddressTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.channelAddressTextBox.Location = new System.Drawing.Point(103, 3);
            this.channelAddressTextBox.Name = "channelAddressTextBox";
            this.channelAddressTextBox.Size = new System.Drawing.Size(148, 20);
            this.channelAddressTextBox.TabIndex = 6;
            // 
            // channelRackLabel
            // 
            this.channelRackLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.channelRackLabel.AutoSize = true;
            this.channelRackLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelRackLabel.Location = new System.Drawing.Point(61, 34);
            this.channelRackLabel.Name = "channelRackLabel";
            this.channelRackLabel.Size = new System.Drawing.Size(36, 13);
            this.channelRackLabel.TabIndex = 7;
            this.channelRackLabel.Text = "Rack:";
            // 
            // channelSlotLabel
            // 
            this.channelSlotLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.channelSlotLabel.AutoSize = true;
            this.channelSlotLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelSlotLabel.Location = new System.Drawing.Point(69, 60);
            this.channelSlotLabel.Name = "channelSlotLabel";
            this.channelSlotLabel.Size = new System.Drawing.Size(28, 13);
            this.channelSlotLabel.TabIndex = 9;
            this.channelSlotLabel.Text = "Slot:";
            // 
            // channelSlotNumericUpDown
            // 
            this.channelSlotNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.channelSlotNumericUpDown.AutoSize = true;
            this.channelSlotNumericUpDown.Location = new System.Drawing.Point(103, 30);
            this.channelSlotNumericUpDown.Name = "channelSlotNumericUpDown";
            this.channelSlotNumericUpDown.Size = new System.Drawing.Size(41, 20);
            this.channelSlotNumericUpDown.TabIndex = 10;
            // 
            // channelRackNumericUpDown
            // 
            this.channelRackNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.channelRackNumericUpDown.AutoSize = true;
            this.channelRackNumericUpDown.Location = new System.Drawing.Point(103, 57);
            this.channelRackNumericUpDown.Name = "channelRackNumericUpDown";
            this.channelRackNumericUpDown.Size = new System.Drawing.Size(41, 20);
            this.channelRackNumericUpDown.TabIndex = 8;
            // 
            // channelDeviceTypeLabel
            // 
            this.channelDeviceTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.channelDeviceTypeLabel.AutoSize = true;
            this.channelDeviceTypeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelDeviceTypeLabel.Location = new System.Drawing.Point(299, 7);
            this.channelDeviceTypeLabel.Name = "channelDeviceTypeLabel";
            this.channelDeviceTypeLabel.Size = new System.Drawing.Size(71, 13);
            this.channelDeviceTypeLabel.TabIndex = 11;
            this.channelDeviceTypeLabel.Text = "Device Type:";
            // 
            // channelDeviceTypeComboBox
            // 
            this.channelDeviceTypeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.channelDeviceTypeComboBox.DisplayMember = "Value";
            this.channelDeviceTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.channelDeviceTypeComboBox.FormattingEnabled = true;
            this.channelDeviceTypeComboBox.Location = new System.Drawing.Point(376, 3);
            this.channelDeviceTypeComboBox.Name = "channelDeviceTypeComboBox";
            this.channelDeviceTypeComboBox.Size = new System.Drawing.Size(150, 21);
            this.channelDeviceTypeComboBox.TabIndex = 12;
            this.channelDeviceTypeComboBox.ValueMember = "Value";
            // 
            // channelChannelTypeLabel
            // 
            this.channelChannelTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.channelChannelTypeLabel.AutoSize = true;
            this.channelChannelTypeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelChannelTypeLabel.Location = new System.Drawing.Point(294, 34);
            this.channelChannelTypeLabel.Name = "channelChannelTypeLabel";
            this.channelChannelTypeLabel.Size = new System.Drawing.Size(76, 13);
            this.channelChannelTypeLabel.TabIndex = 13;
            this.channelChannelTypeLabel.Text = "Channel Type:";
            // 
            // timeoutBreakDetectionCheckBox
            // 
            this.timeoutBreakDetectionCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.timeoutBreakDetectionCheckBox.AutoSize = true;
            this.timeoutBreakDetectionCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.timeoutBreakDetectionCheckBox.Location = new System.Drawing.Point(267, 58);
            this.timeoutBreakDetectionCheckBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.timeoutBreakDetectionCheckBox.Name = "timeoutBreakDetectionCheckBox";
            this.timeoutBreakDetectionCheckBox.Size = new System.Drawing.Size(106, 17);
            this.timeoutBreakDetectionCheckBox.TabIndex = 25;
            this.timeoutBreakDetectionCheckBox.Text = "Break Detection:";
            this.timeoutBreakDetectionCheckBox.UseVisualStyleBackColor = true;
            // 
            // testConfigurationButton
            // 
            this.testConfigurationButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.testConfigurationButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.testConfigurationButton.Location = new System.Drawing.Point(376, 93);
            this.testConfigurationButton.Name = "testConfigurationButton";
            this.testConfigurationButton.Size = new System.Drawing.Size(75, 23);
            this.testConfigurationButton.TabIndex = 18;
            this.testConfigurationButton.Text = "Device Info";
            this.testConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // timeoutBreakDetectionPanel
            // 
            this.timeoutBreakDetectionPanel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.timeoutBreakDetectionPanel.Controls.Add(this.timeoutBreakDetectionMillisecondsLabel);
            this.timeoutBreakDetectionPanel.Controls.Add(this.timeoutBreakDetectionNumericUpDown);
            this.timeoutBreakDetectionPanel.Location = new System.Drawing.Point(376, 55);
            this.timeoutBreakDetectionPanel.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.timeoutBreakDetectionPanel.Name = "timeoutBreakDetectionPanel";
            this.timeoutBreakDetectionPanel.Size = new System.Drawing.Size(109, 23);
            this.timeoutBreakDetectionPanel.TabIndex = 27;
            // 
            // timeoutBreakDetectionMillisecondsLabel
            // 
            this.timeoutBreakDetectionMillisecondsLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.timeoutBreakDetectionMillisecondsLabel.AutoSize = true;
            this.timeoutBreakDetectionMillisecondsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.timeoutBreakDetectionMillisecondsLabel.Location = new System.Drawing.Point(72, 5);
            this.timeoutBreakDetectionMillisecondsLabel.Name = "timeoutBreakDetectionMillisecondsLabel";
            this.timeoutBreakDetectionMillisecondsLabel.Size = new System.Drawing.Size(20, 13);
            this.timeoutBreakDetectionMillisecondsLabel.TabIndex = 27;
            this.timeoutBreakDetectionMillisecondsLabel.Text = "ms";
            // 
            // timeoutBreakDetectionNumericUpDown
            // 
            this.timeoutBreakDetectionNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.timeoutBreakDetectionNumericUpDown.Location = new System.Drawing.Point(0, 1);
            this.timeoutBreakDetectionNumericUpDown.Name = "timeoutBreakDetectionNumericUpDown";
            this.timeoutBreakDetectionNumericUpDown.Size = new System.Drawing.Size(70, 20);
            this.timeoutBreakDetectionNumericUpDown.TabIndex = 9;
            // 
            // channelChannelTypeComboBox
            // 
            this.channelChannelTypeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.channelChannelTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.channelChannelTypeComboBox.FormattingEnabled = true;
            this.channelChannelTypeComboBox.Location = new System.Drawing.Point(376, 30);
            this.channelChannelTypeComboBox.Name = "channelChannelTypeComboBox";
            this.channelChannelTypeComboBox.Size = new System.Drawing.Size(150, 21);
            this.channelChannelTypeComboBox.TabIndex = 14;
            // 
            // channelGroupBox
            // 
            this.channelGroupBox.Controls.Add(this.channelTableLayoutPanel);
            this.channelGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.channelGroupBox.Location = new System.Drawing.Point(3, 28);
            this.channelGroupBox.Name = "channelGroupBox";
            this.channelGroupBox.Size = new System.Drawing.Size(548, 139);
            this.channelGroupBox.TabIndex = 95;
            this.channelGroupBox.TabStop = false;
            this.channelGroupBox.Text = "Channel";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 509);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(554, 22);
            this.statusStrip.TabIndex = 96;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusToolStripStatusLabel
            // 
            this.statusToolStripStatusLabel.Name = "statusToolStripStatusLabel";
            this.statusToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openConnectToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 170);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(554, 25);
            this.toolStrip.TabIndex = 97;
            this.toolStrip.Text = "toolStrip1";
            // 
            // openConnectToolStripButton
            // 
            this.openConnectToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openConnectToolStripButton.Image")));
            this.openConnectToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openConnectToolStripButton.Name = "openConnectToolStripButton";
            this.openConnectToolStripButton.Size = new System.Drawing.Size(112, 22);
            this.openConnectToolStripButton.Text = "Open / Connect";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showAboutToolStripButton,
            this.showHelpToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(554, 25);
            this.toolStrip1.TabIndex = 98;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // showAboutToolStripButton
            // 
            this.showAboutToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.showAboutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showAboutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("showAboutToolStripButton.Image")));
            this.showAboutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showAboutToolStripButton.Name = "showAboutToolStripButton";
            this.showAboutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.showAboutToolStripButton.Text = "About";
            // 
            // showHelpToolStripButton
            // 
            this.showHelpToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.showHelpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showHelpToolStripButton.Image = global::Interactive.Properties.Resources.Help;
            this.showHelpToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showHelpToolStripButton.Name = "showHelpToolStripButton";
            this.showHelpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.showHelpToolStripButton.Text = "Help";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.toolStrip1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.toolStrip, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.channelGroupBox, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.readDataGroupBox, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.dataGridView, 0, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(554, 509);
            this.tableLayoutPanel.TabIndex = 99;
            // 
            // readDataGroupBox
            // 
            this.readDataGroupBox.Controls.Add(this.addressTableLayoutPanel);
            this.readDataGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.readDataGroupBox.Location = new System.Drawing.Point(3, 198);
            this.readDataGroupBox.Name = "readDataGroupBox";
            this.readDataGroupBox.Size = new System.Drawing.Size(548, 143);
            this.readDataGroupBox.TabIndex = 99;
            this.readDataGroupBox.TabStop = false;
            this.readDataGroupBox.Text = "Read Data";
            // 
            // addressTableLayoutPanel
            // 
            this.addressTableLayoutPanel.ColumnCount = 8;
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.addressTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.addressTableLayoutPanel.Controls.Add(this.readAddressLabel, 0, 3);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressDataAreaComboBox, 1, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressDataAreaLabel, 1, 1);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressDataBlockNumberLabel, 2, 1);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressDataBlockNumberNumericUpDown, 2, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressTextBox, 1, 3);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressByteNumberLabel, 4, 1);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressByteNumberNumericUpDown, 4, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressBitNumberLabel, 5, 1);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressBitNumberNumericUpDown, 5, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressRawTypeNumberLabel, 3, 1);
            this.addressTableLayoutPanel.Controls.Add(this.readAddressRawTypeComboBox, 3, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readCountNumericUpDown, 6, 3);
            this.addressTableLayoutPanel.Controls.Add(this.readCountLabel, 6, 2);
            this.addressTableLayoutPanel.Controls.Add(this.readTypeLabel, 0, 5);
            this.addressTableLayoutPanel.Controls.Add(this.readTypeComboBox, 1, 5);
            this.addressTableLayoutPanel.Controls.Add(this.readButton, 2, 4);
            this.addressTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.addressTableLayoutPanel.Name = "addressTableLayoutPanel";
            this.addressTableLayoutPanel.RowCount = 7;
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.addressTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.addressTableLayoutPanel.Size = new System.Drawing.Size(542, 124);
            this.addressTableLayoutPanel.TabIndex = 0;
            // 
            // readAddressLabel
            // 
            this.readAddressLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.readAddressLabel.AutoSize = true;
            this.readAddressLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressLabel.Location = new System.Drawing.Point(49, 56);
            this.readAddressLabel.Name = "readAddressLabel";
            this.readAddressLabel.Size = new System.Drawing.Size(48, 13);
            this.readAddressLabel.TabIndex = 16;
            this.readAddressLabel.Text = "Address:";
            // 
            // readAddressDataAreaComboBox
            // 
            this.readAddressDataAreaComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressDataAreaComboBox.DisplayMember = "Value";
            this.readAddressDataAreaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.readAddressDataAreaComboBox.FormattingEnabled = true;
            this.readAddressDataAreaComboBox.Location = new System.Drawing.Point(103, 26);
            this.readAddressDataAreaComboBox.Name = "readAddressDataAreaComboBox";
            this.readAddressDataAreaComboBox.Size = new System.Drawing.Size(101, 21);
            this.readAddressDataAreaComboBox.TabIndex = 13;
            this.readAddressDataAreaComboBox.ValueMember = "Value";
            // 
            // readAddressDataAreaLabel
            // 
            this.readAddressDataAreaLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressDataAreaLabel.AutoSize = true;
            this.readAddressDataAreaLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressDataAreaLabel.Location = new System.Drawing.Point(103, 10);
            this.readAddressDataAreaLabel.Name = "readAddressDataAreaLabel";
            this.readAddressDataAreaLabel.Size = new System.Drawing.Size(55, 13);
            this.readAddressDataAreaLabel.TabIndex = 6;
            this.readAddressDataAreaLabel.Text = "Data Area";
            // 
            // readAddressDataBlockNumberLabel
            // 
            this.readAddressDataBlockNumberLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressDataBlockNumberLabel.AutoSize = true;
            this.readAddressDataBlockNumberLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressDataBlockNumberLabel.Location = new System.Drawing.Point(210, 10);
            this.readAddressDataBlockNumberLabel.Name = "readAddressDataBlockNumberLabel";
            this.readAddressDataBlockNumberLabel.Size = new System.Drawing.Size(36, 13);
            this.readAddressDataBlockNumberLabel.TabIndex = 14;
            this.readAddressDataBlockNumberLabel.Text = "DB Nr";
            // 
            // readAddressDataBlockNumberNumericUpDown
            // 
            this.readAddressDataBlockNumberNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressDataBlockNumberNumericUpDown.Location = new System.Drawing.Point(210, 26);
            this.readAddressDataBlockNumberNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.readAddressDataBlockNumberNumericUpDown.Name = "readAddressDataBlockNumberNumericUpDown";
            this.readAddressDataBlockNumberNumericUpDown.Size = new System.Drawing.Size(57, 20);
            this.readAddressDataBlockNumberNumericUpDown.TabIndex = 15;
            this.readAddressDataBlockNumberNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // readAddressTextBox
            // 
            this.readAddressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.readAddressTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.addressTableLayoutPanel.SetColumnSpan(this.readAddressTextBox, 5);
            this.readAddressTextBox.Location = new System.Drawing.Point(103, 53);
            this.readAddressTextBox.Name = "readAddressTextBox";
            this.readAddressTextBox.Size = new System.Drawing.Size(333, 20);
            this.readAddressTextBox.TabIndex = 17;
            // 
            // readAddressByteNumberLabel
            // 
            this.readAddressByteNumberLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressByteNumberLabel.AutoSize = true;
            this.readAddressByteNumberLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressByteNumberLabel.Location = new System.Drawing.Point(339, 10);
            this.readAddressByteNumberLabel.Name = "readAddressByteNumberLabel";
            this.readAddressByteNumberLabel.Size = new System.Drawing.Size(42, 13);
            this.readAddressByteNumberLabel.TabIndex = 18;
            this.readAddressByteNumberLabel.Text = "Byte Nr";
            // 
            // readAddressByteNumberNumericUpDown
            // 
            this.readAddressByteNumberNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressByteNumberNumericUpDown.Location = new System.Drawing.Point(339, 26);
            this.readAddressByteNumberNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.readAddressByteNumberNumericUpDown.Name = "readAddressByteNumberNumericUpDown";
            this.readAddressByteNumberNumericUpDown.Size = new System.Drawing.Size(57, 20);
            this.readAddressByteNumberNumericUpDown.TabIndex = 19;
            this.readAddressByteNumberNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // readAddressBitNumberLabel
            // 
            this.readAddressBitNumberLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressBitNumberLabel.AutoSize = true;
            this.readAddressBitNumberLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressBitNumberLabel.Location = new System.Drawing.Point(402, 10);
            this.readAddressBitNumberLabel.Name = "readAddressBitNumberLabel";
            this.readAddressBitNumberLabel.Size = new System.Drawing.Size(33, 13);
            this.readAddressBitNumberLabel.TabIndex = 20;
            this.readAddressBitNumberLabel.Text = "Bit Nr";
            // 
            // readAddressBitNumberNumericUpDown
            // 
            this.readAddressBitNumberNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressBitNumberNumericUpDown.Location = new System.Drawing.Point(402, 26);
            this.readAddressBitNumberNumericUpDown.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.readAddressBitNumberNumericUpDown.Name = "readAddressBitNumberNumericUpDown";
            this.readAddressBitNumberNumericUpDown.Size = new System.Drawing.Size(34, 20);
            this.readAddressBitNumberNumericUpDown.TabIndex = 21;
            // 
            // readAddressRawTypeNumberLabel
            // 
            this.readAddressRawTypeNumberLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressRawTypeNumberLabel.AutoSize = true;
            this.readAddressRawTypeNumberLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readAddressRawTypeNumberLabel.Location = new System.Drawing.Point(273, 10);
            this.readAddressRawTypeNumberLabel.Name = "readAddressRawTypeNumberLabel";
            this.readAddressRawTypeNumberLabel.Size = new System.Drawing.Size(56, 13);
            this.readAddressRawTypeNumberLabel.TabIndex = 24;
            this.readAddressRawTypeNumberLabel.Text = "Raw Type";
            // 
            // readAddressRawTypeComboBox
            // 
            this.readAddressRawTypeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readAddressRawTypeComboBox.DisplayMember = "Value";
            this.readAddressRawTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.readAddressRawTypeComboBox.FormattingEnabled = true;
            this.readAddressRawTypeComboBox.Location = new System.Drawing.Point(273, 26);
            this.readAddressRawTypeComboBox.Name = "readAddressRawTypeComboBox";
            this.readAddressRawTypeComboBox.Size = new System.Drawing.Size(60, 21);
            this.readAddressRawTypeComboBox.TabIndex = 25;
            this.readAddressRawTypeComboBox.ValueMember = "Value";
            // 
            // readCountNumericUpDown
            // 
            this.readCountNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readCountNumericUpDown.Location = new System.Drawing.Point(442, 53);
            this.readCountNumericUpDown.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.readCountNumericUpDown.Name = "readCountNumericUpDown";
            this.readCountNumericUpDown.Size = new System.Drawing.Size(54, 20);
            this.readCountNumericUpDown.TabIndex = 23;
            // 
            // readCountLabel
            // 
            this.readCountLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.readCountLabel.AutoSize = true;
            this.readCountLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readCountLabel.Location = new System.Drawing.Point(442, 30);
            this.readCountLabel.Name = "readCountLabel";
            this.readCountLabel.Size = new System.Drawing.Size(35, 13);
            this.readCountLabel.TabIndex = 22;
            this.readCountLabel.Text = "Count";
            // 
            // readTypeLabel
            // 
            this.readTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.readTypeLabel.AutoSize = true;
            this.readTypeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readTypeLabel.Location = new System.Drawing.Point(63, 93);
            this.readTypeLabel.Name = "readTypeLabel";
            this.readTypeLabel.Size = new System.Drawing.Size(34, 13);
            this.readTypeLabel.TabIndex = 26;
            this.readTypeLabel.Text = "Type:";
            // 
            // readTypeComboBox
            // 
            this.readTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.readTypeComboBox.DisplayMember = "Value";
            this.readTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.readTypeComboBox.FormattingEnabled = true;
            this.readTypeComboBox.Location = new System.Drawing.Point(103, 89);
            this.readTypeComboBox.Name = "readTypeComboBox";
            this.readTypeComboBox.Size = new System.Drawing.Size(101, 21);
            this.readTypeComboBox.TabIndex = 27;
            this.readTypeComboBox.ValueMember = "Value";
            // 
            // readButton
            // 
            this.readButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.addressTableLayoutPanel.SetColumnSpan(this.readButton, 2);
            this.readButton.Image = global::Interactive.Properties.Resources.ReadFromPlc;
            this.readButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.readButton.Location = new System.Drawing.Point(210, 87);
            this.readButton.Name = "readButton";
            this.addressTableLayoutPanel.SetRowSpan(this.readButton, 3);
            this.readButton.Size = new System.Drawing.Size(87, 25);
            this.readButton.TabIndex = 28;
            this.readButton.Text = "Read Data";
            this.readButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.readButton.UseVisualStyleBackColor = true;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(3, 347);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(548, 159);
            this.dataGridView.TabIndex = 100;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 531);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.statusStrip);
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IP-S7-Link .NET Advanced - Interactive";
            this.channelTableLayoutPanel.ResumeLayout(false);
            this.channelTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channelSlotNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelRackNumericUpDown)).EndInit();
            this.timeoutBreakDetectionPanel.ResumeLayout(false);
            this.timeoutBreakDetectionPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeoutBreakDetectionNumericUpDown)).EndInit();
            this.channelGroupBox.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.readDataGroupBox.ResumeLayout(false);
            this.addressTableLayoutPanel.ResumeLayout(false);
            this.addressTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressDataBlockNumberNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressByteNumberNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readAddressBitNumberNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewCheckBoxColumn isMdiContainerDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn keyPreviewDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn opacityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn textDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rightToLeftLayoutDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn topMostDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startPositionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transparencyKeyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showInTaskbarDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeGripStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showIconDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn iconDataGridViewImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn helpButtonDataGridViewCheckBoxColumn;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn maximumSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn acceptButtonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn minimizeBoxDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Timer StatusTimer;
        private System.Windows.Forms.DataGridViewTextBoxColumn minimumSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn maximizeBoxDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn formBorderStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mainMenuStripDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cancelButtonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn allowDropDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn backgroundImageLayoutDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autoScrollMinSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accessibleRoleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accessibleDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn causesValidationDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accessibleNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn foreColorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn contextMenuStripDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rightToLeftDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tagDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cursorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn enabledDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataBindingsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn backgroundImageDataGridViewImageColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn useWaitCursorDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn backColorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn controlBoxDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn autoScrollDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autoValidateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn autoSizeDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn windowStateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autoSizeModeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn anchorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn autoScrollMarginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imeModeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paddingDataGridViewTextBoxColumn;
        private System.Windows.Forms.HelpProvider helpProvider;
        private System.Windows.Forms.DataGridViewCheckBoxColumn visibleDataGridViewCheckBoxColumn;
        private System.Windows.Forms.TableLayoutPanel channelTableLayoutPanel;
        private System.Windows.Forms.Label channelAddressLabel;
        private System.Windows.Forms.TextBox channelAddressTextBox;
        private System.Windows.Forms.Label channelRackLabel;
        private System.Windows.Forms.GroupBox channelGroupBox;
        private System.Windows.Forms.Label channelChannelTypeLabel;
        private System.Windows.Forms.NumericUpDown channelRackNumericUpDown;
        private System.Windows.Forms.Label channelSlotLabel;
        private System.Windows.Forms.NumericUpDown channelSlotNumericUpDown;
        private System.Windows.Forms.Label channelDeviceTypeLabel;
        private System.Windows.Forms.ComboBox channelDeviceTypeComboBox;
        private System.Windows.Forms.ComboBox channelChannelTypeComboBox;
        private System.Windows.Forms.CheckBox timeoutBreakDetectionCheckBox;
        private System.Windows.Forms.Panel timeoutBreakDetectionPanel;
        private System.Windows.Forms.Label timeoutBreakDetectionMillisecondsLabel;
        private System.Windows.Forms.NumericUpDown timeoutBreakDetectionNumericUpDown;
        private System.Windows.Forms.Button testConfigurationButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusToolStripStatusLabel;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton openConnectToolStripButton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton showAboutToolStripButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.GroupBox readDataGroupBox;
        private System.Windows.Forms.TableLayoutPanel addressTableLayoutPanel;
        private System.Windows.Forms.ToolStripButton showHelpToolStripButton;
        private System.Windows.Forms.ComboBox readAddressDataAreaComboBox;
        private System.Windows.Forms.Label readAddressDataAreaLabel;
        private System.Windows.Forms.Label readAddressDataBlockNumberLabel;
        private System.Windows.Forms.NumericUpDown readAddressDataBlockNumberNumericUpDown;
        private System.Windows.Forms.Label readAddressLabel;
        private System.Windows.Forms.TextBox readAddressTextBox;
        private System.Windows.Forms.Label readAddressByteNumberLabel;
        private System.Windows.Forms.NumericUpDown readAddressByteNumberNumericUpDown;
        private System.Windows.Forms.Label readAddressBitNumberLabel;
        private System.Windows.Forms.NumericUpDown readAddressBitNumberNumericUpDown;
        private System.Windows.Forms.Label readCountLabel;
        private System.Windows.Forms.NumericUpDown readCountNumericUpDown;
        private System.Windows.Forms.Label readAddressRawTypeNumberLabel;
        private System.Windows.Forms.ComboBox readAddressRawTypeComboBox;
        private System.Windows.Forms.Label readTypeLabel;
        private System.Windows.Forms.ComboBox readTypeComboBox;
        private System.Windows.Forms.Button readButton;
        private System.Windows.Forms.DataGridView dataGridView;
    }
}


﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace Interactive
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using IPS7Lnk.Advanced;

    public partial class MainView : Form
    {
        #region ---------- Private fields ----------

        private PlcDeviceConnection connection;
        private PlcDevice device;

        #endregion

        #region ---------- Public constructors ----------

        public MainView()
            : base()
        {
            this.InitializeComponent();
            this.InitializeComboBoxes();
        }

        #endregion

        #region ---------- Private methods ----------

        private void InitializeComboBoxes()
        {
            this.channelDeviceTypeComboBox.Items.AddRange(new object[] {
                SiemensDeviceType.Logo,
                SiemensDeviceType.S7200,
                SiemensDeviceType.S7300_400,
                SiemensDeviceType.S71200,
                SiemensDeviceType.S71500
            });

            this.channelDeviceTypeComboBox.SelectedItem = SiemensDeviceType.S7300_400;

            this.channelChannelTypeComboBox.Items.AddRange(new object[] {
                PlcDeviceConnectionType.OperationPanel,
                PlcDeviceConnectionType.ProgrammerDevice,
                PlcDeviceConnectionType.Other
            });

            this.channelChannelTypeComboBox.SelectedItem = PlcDeviceConnectionType.OperationPanel;

            this.readAddressDataAreaComboBox.Items.AddRange(new object[] {
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.Input, 
                        string.Format("{0} ({1})", SiemensOperandTypes.Input, PlcOperandType.Input)),
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.Output,
                        string.Format("{0} ({1})", SiemensOperandTypes.Output, PlcOperandType.Output)),
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.Flag,
                        string.Format("{0} ({1})", SiemensOperandTypes.Flag, PlcOperandType.Flag)),
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.DataBlock,
                        string.Format("{0} ({1})", SiemensOperandTypes.DataBlock, PlcOperandType.DataBlock)),
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.Timer,
                        string.Format("{0} ({1})", SiemensOperandTypes.Timer, PlcOperandType.Timer)),
                new KeyValuePair<PlcOperandType, string>(
                        PlcOperandType.Counter,
                        string.Format("{0} ({1})", SiemensOperandTypes.Counter, PlcOperandType.Counter)),
            });

            this.readAddressDataAreaComboBox.DisplayMember = "Value";
            this.readAddressDataAreaComboBox.ValueMember = "Key";
            this.readAddressDataAreaComboBox.SelectedValue = PlcOperandType.DataBlock;

            this.readAddressRawTypeComboBox.Items.AddRange(new object[] {
                PlcRawType.Bit,
                PlcRawType.Byte,
                PlcRawType.Word,
                PlcRawType.DWord
            });

            this.readAddressRawTypeComboBox.SelectedItem = PlcRawType.Byte;

            this.readTypeComboBox.Items.AddRange(new object[] {
                PlcTypeCode.Bool,
                PlcTypeCode.Byte,
                PlcTypeCode.Real,
                PlcTypeCode.Int,
                PlcTypeCode.DInt,
                PlcTypeCode.Word,
                PlcTypeCode.DWord,
                PlcTypeCode.String
            });

            this.readTypeComboBox.SelectedItem = PlcTypeCode.Byte;
        }

        #endregion
    }
}

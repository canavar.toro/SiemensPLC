﻿% Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.
% This sample demonstrates how to work with the 'PlcAddress' class.

% The PlcAddress class itself does represent an immutable type which
% describes the metadata stored within a typical compound string value used
% to access the different data areas defined within a PLC device.

% 1. Way: Pure coded ctor use.
% The use of the ctor is most useful in scenarios there the address needs
% to be generated during the runtime of your toolbox/script. So you are not
% forced to generate the compound string value of the address manually.

% The following example results into the address: 'DB100.DBX 1.0'.
address1 = PlcAddress(PlcOperand.DataBlock(100), PlcRawType.Bit, 1, 0);

% If you want to address a different data area there you do not need to
% specify the number of the area like the PERIPHERY INOUT block you just
% have to leave the second parameter in the ctor.
% The following example results into the address: 'E 1.0'.
address2 = PlcAddress(PlcOperand.Input, PlcRawType.Bit, 1, 0);

% 2. Way: Parse existing compound string values to an address.
% (NOT YET SUPPORTED in MatLab)
% The use of the parse methods is useful in scenarios there you may ensure
% that a custom address string is valid and does maybe refer to the
% expected metadata.

% You can either use the parse method (which throws an exception if the
% format of the address string is invalid).
% address1 = PlcAddress.Parse('DB100.DBX 1.0');

% Or you use the try parse method (which tries to parse the address
% string). On success the resulting address is stored within the out
% parameter.
% success = PlcAddress.TryParse('E 1.0', out address2);

% 3. Way: Implicit cast operator.
% To reduce the partially cumbersome configuration of an address for e.g.
% via ctor you can just create a new PlcAddress using the implicit cast
% operator provided by the class. Note: The operator itself does internally
% use the parse method, which means initializing a new instance with an
% invalid formatted address using the implicit cast operator will end into
% a format exception. In general does this method provide the most
% comfortably way (especially for those used to the PLC address format).

% Note: This functionality is provided by each method within the
%       IPS7LnkNet.Advanced.MatLab toolbox.
address1 = 'DB100.DBX 1.0';
address2 = 'E 1.0';

% The mechanism used by the PlcAddress class to interpret address strings
% does provide a wide range of flexibility.

% 1. Way: Chaos addresses.
% The following 'chaos' addresses are correctly interpreted by the
% PlcAddress class without to miss any information within the malformed
% address strings.

% Note: This functionality is provided by each method within the
%       IPS7LnkNet.Advanced.MatLab toolbox.

% The following example results into the address: 'DB100.DBX 1.0'.
address1 = ' dB 100  . Db 1    . 0  ';

% The following example results into the address: 'E 1.0'.
address2 = ' E x 1  ';


% 2. Way: Siemens and IEC conform address support.
% While Siemes and the IEC do not completly match within the address
% formats specified in their specifications, the use of the PlcAddress does
% support both specifications.

% Note: This functionality is provided by each method within the
%       IPS7LnkNet.Advanced.MatLab toolbox.

% You can define an address either in Siemens notation.
address1 = 'E 1.0';

% Or in IEC notation.
address2 = 'I 1.0';

% By default does the PlcAddress class format the address metadata into the
% Siemens format but using a different ToString overload you can specify
% the desired standard to format the address.

% The following example results into the address: 'I 1.0'.
% (NOT YET SUPPORTED in MatLab)
% addressStringAsIec = address1.ToString(PlcOperandStandard.IEC);

% The following example results into the address: 'E 1.0'.
% (NOT YET SUPPORTED in MatLab)
% addressStringAsSiemens = address2.ToString(PlcOperandStandard.Siemens);

% Additional operators/methods provided by the PlcAddress class support the
% use of PLC addresses like numerical values.

% 1. Way: Comparison operators. (NOT YET SUPPORTED in MatLab)
% As soon both addresses are within the same data area they can be easly
% compared with each other. Otherwise an address of a different data area
% will be always greater or lower than an address refering to a different
% data area.
% In general addresses from different data areas are compared/sorted within
% the following order:
% Input, PeripheryInput, Output, PeripheryOutput, Flag, DataBlock,
% Counter, Timer.

address1 = 'DB100.DBB 10';
address2 = 'DB100.DBB 20';

% You can check whether one of them is lower.
% (NOT YET SUPPORTED in MatLab)
% if (address1 < address2)
%     address2 = address1;
% end

% You can check whether both are equals.
% (NOT YET SUPPORTED in MatLab)
% if (address1 == address2)
%     address2 = 'DB100.DBB 4';
% end

% You can check whether one of them is greater.
% (NOT YET SUPPORTED in MatLab)
% if (address1 > address2)
%     address2 = address1;
% end

% Additionally to the represented operators there are also implemented
% <= and >= to compare two addresses with each other.

% 2. Way: Sorting support. (NOT YET SUPPORTED in MatLab)
  
% Instances of the PlcAddress class do also support sorting mechanism
% through implementing the required default framework interfaces.
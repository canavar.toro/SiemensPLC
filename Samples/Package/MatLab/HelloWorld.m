﻿% Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

% This sample demonstrates a Hello World! application.

% This script does write/read the 'Hello World!' message to/from the PLC and when
% prints the message on the standard output.

device = SiemensDevice( ...
	IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S7300_400);

connection = device.CreateConnection();
connection.Open();

connection.WriteString('DB111.DBB 100', 'Hello World!');

message = connection.ReadString('DB111.DBB 100', 16);
message

connection.Close();

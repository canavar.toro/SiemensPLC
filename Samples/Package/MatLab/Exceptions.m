﻿% Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

% This sample demonstrates the different mechanisms provided to handle exceptions.

device = SiemensDevice( ...
	IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S7300_400);

connection = device.CreateConnection();
connection.Open();

% 1. Way: Default Try-Catch-(Finally) block.

% An exception can occur upon different reasons, for e.g. either the IP address
% of the PLC device is wrong, the PLC itself is not available or the PLC address
% specified (like in the following example) is invalid. In such cases will then
% an exception of the type 'PlcException' thrown.

try
    connection.ReadInt16('DB111.DBD 6');
catch ex
    ex.message
    ex.identifier
end

% 2. Way: Global status validation (via connection).
% (NOT YET SUPPORTED in MatLab)

% The default status validation which will always throw an exception in cases
% there the status does indicate a PlcStatusCode un-equals to
% PlcStatusCode.NoError can be overridden by a custom callback. As soon a custom
% callback has been registered (like in the following example) there will be no
% longer an exception thrown. The callback itself is always invoked when a
% status of a driver based object has been changed and needs therefore to be
% validated. A status change does not implicity indicate an internal error.
%
% This way does demonstrate that the connection will be passed to the global
% validation callback.

% PlcNotifications.ValidateStatus = Program.ValidateStatus;
% connection.ReadInt16('DB111.DBD 6');

% 3. Way: Global status validation (via value).
% (NOT YET SUPPORTED in MatLab)

% The default status validation which will always throw an exception in cases
% there the status does indicate a PlcStatusCode un-equals to
% PlcStatusCode.NoError can be overridden by a custom callback. As soon a custom
% callback has been registered (like in the following example) there will be no
% longer an exception thrown. The callback itself is always invoked when a
% status of a driver based object has been changed and needs therefore to be
% validated. A status change does not implicity indicate an internal error.
%
% This way does demonstrate that the value object will be passed to the global
% validation callback.

% PlcNotifications.ValidateStatus = Program.ValidateStatus;

% value = PlcInt16('DB111.DBX 6.0');
% connection.ReadValues(value);

% ValidateStatus(provider)
    % The IPlcStatusProvider interface is implemented by the IPlcValue interface and
    % PlcDeviceConnection class. This does not only allow to determine and validate the
    % status of the object affected by a status change, it does also support the re-use
    % of the affected object to provide additional custom validation mechanism (like
    % in the code below).

    % provider.Status.Text
    % provider.Status.Code
﻿% Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

% This sample demonstrates how to work with the different device types
% supported by the framework.

% Overall there is not really any special knowledge required to establish a
% connection to a different Siemens device type. As you will see in the
% following snippets there does only differ one argument when initializing
% a new device instance or a single property set call to change the device
% type.

% 1. Way: Default device type
% The simplest and the most general way is to just initialize a device
% using the constructor accepting the end point information. Using this
% constructor will result into a device object which can be used to access
% S7-300 and S7-400 PLC devices.

device = SiemensDevice(IPDeviceEndPoint('192.168.0.80'));
device.Type

% 2. Way: Explicit device type
% The advanced way would be to initialize a new device object using the
% constructor which besides of an end point does also accept device type
% information.

device1 = SiemensDevice( ...
    IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.Logo);

device1.Type

device2 = SiemensDevice( ...
    IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S7300_400);

device2.Type

device3 = SiemensDevice( ...
    IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S71200);

device3.Type

device4 = SiemensDevice( ...
	IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S71500);

device4.Type

% 3. Way: Late device type (re-)definition
% Independent from the way how you decide to initialize your device object
% you are always able to change the device type at runtime.

device1 = SiemensDevice( ...
IPDeviceEndPoint('192.168.0.80'));

device1.Type = SiemensDeviceType.S71500;
device1.Type

device2 = SiemensDevice( ...
		IPDeviceEndPoint('192.168.0.80'), SiemensDeviceType.S71500);

device2.Type = SiemensDeviceType.S7300_400;
device2.Type

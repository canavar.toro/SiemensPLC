= Copyright =
Traeger Industry Components GmbH
This file is protected by Trager Industry Components Copyright (c) 2013-2015.

www.traeger.de
info@traeger.de

S�llnerstr. 9
D-92637 Weiden

Phone  +49 961 482300
Mobile +49 171 83 91 340
Fax    +49 961 48230-20


= German =
Bevor Sie das IPS7LnkNet.Advanced Framework nutzen k�nnen, m�ssen Sie die folgenden Schritte
zur Installation des Frameworks durchf�hren:
* Entpacken Sie das IPS7LnkNet.Advanced.zip Archiv, welches sich neben dieser Readme Datei befindet.
* Lokalisieren Sie die IPS7LnkNet.Advanced.dll unterhalb des entpackten Ordners.
* Binden Sie die Assembly in ihr gew�nschtes .NET Projekt ein und legen Sie los.

== Paket Inhalt ==
Das Paket in dem sich dieses Readme Datei befindet umfasst die folgenden Inhalte:
* IPS7LnkNet.chm - Die Dokumentation des vom Framework verwendeten Software Treibers, welcher
  ebenfalls Teil des Frameworks ist.
* IPS7LnkNet.Advanced.dll - Die Framework Assembly.
* IPS7LnkNet.Advanced.xml - Die Framework IDE Dokumentation.
* IPS7LnkNet.Advanced.chm - Die Framework Dokumentation.
* IPS7LnkNet.Advanced DE.pdf - Eine kurze �bersicht �ber das Framework.
* S7Watch.exe - Eine Demo Anwendung um einen kurzen Eindruck vom Framework zu bekommen.
* Demos - Eine Sammlung von produktiven bzw. demonstrativen Anwendungen des Frameworks.
* Samples - Eine Sammlung von Beispielen zur Anwendung des Frameworks.
* Readme.txt - Diese Readme Datei.
* Version.txt - Die Versionshistorie �ber die letzten �nderungen und Neuerungen.

= English =
Before you can use the IPS7LnkNet.Advanced framework you need to perform the following steps
to setup the framework:
* Unpack the IPS7LnkNet.Advanced.zip archive which resides next to this readme file.
* Locate the IPS7LnkNet.Advanced.dll beneath the unpacked folder.
* Reference this assembly in your desired .NET project and enjoy.

== Package Content ==
The package which contains this readme file contains the following additional files:
* IPS7LnkNet.chm - The documentation of the software driver used by the framework which is
  also part of it.
* IPS7LnkNet.Advanced.dll - The framework assembly.
* IPS7LnkNet.Advanced.xml - The framework IDE documentation.
* IPS7LnkNet.Advanced.chm - The framework documentation.
* IPS7LnkNet.Advanced EN.pdf - A short overview about the framework (not yet included).
* S7Watch.exe - A demo application to get a short impression of the framework.
* Demos - A set of productive and demonstrative applications of the framework.
* Samples - A set of samples to present how to use the framework.
* Readme.txt - This readme file.
* Version.txt - The version history about the recent changes and innovations.

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.Text;

    public static class StringHelper
    {
        public static string Partition(string value)
        {
            return StringHelper.Partition(value, 4);
        }

        public static string Partition(string value, int partitionSize)
        {
            return StringHelper.Partition(value, partitionSize, ' ');
        }

        public static string Partition(string value, int partitionSize, char paddingChar)
        {
            int count = (int)Math.Ceiling((double)value.Length / partitionSize);
            var textBuilder = new StringBuilder(count * (partitionSize + 1));

            int startIndex = 0;
            for (int index = count; index > 0; index--) {
                int endPosition = (value.Length - (index * partitionSize)) + partitionSize;

                if (textBuilder.Length != 0)
                    textBuilder.Append(" ");

                var partition = value.Substring(startIndex, endPosition - startIndex);
                textBuilder.Append(partition.PadLeft(partitionSize, paddingChar));

                startIndex = endPosition;
            }

            return textBuilder.ToString();
        }
    }
}
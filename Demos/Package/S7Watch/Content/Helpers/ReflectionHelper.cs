﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public static class ReflectionHelper
    {
        #region ---------- Public static methods ----------

        public static Attribute GetAttribute(ICustomAttributeProvider provider)
        {
            return ReflectionHelper.GetAttribute(provider, false);
        }

        public static Attribute GetAttribute(ICustomAttributeProvider provider, bool inherit)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            object[] attributes = provider.GetCustomAttributes(inherit);

            if (attributes.Length == 0)
                return null;

            return (Attribute)attributes[0];
        }

        public static T GetAttribute<T>(ICustomAttributeProvider provider)
            where T : Attribute
        {
            return ReflectionHelper.GetAttribute<T>(provider, false);
        }

        public static T GetAttribute<T>(ICustomAttributeProvider provider, bool inherit)
            where T : Attribute
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            object[] attributes = provider.GetCustomAttributes(typeof(T), inherit);

            if (attributes.Length == 0)
                return null;

            return (T)attributes[0];
        }

        public static IEnumerable<Attribute> GetAttributes(ICustomAttributeProvider provider)
        {
            return ReflectionHelper.GetAttributes(provider, false);
        }

        public static IEnumerable<Attribute> GetAttributes(
                ICustomAttributeProvider provider,
                bool inherit)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            object[] attributes = provider.GetCustomAttributes(inherit);

            for (int index = 0; index < attributes.Length; index++)
                yield return (Attribute)attributes[index];
        }

        public static IEnumerable<T> GetAttributes<T>(ICustomAttributeProvider provider)
            where T : Attribute
        {
            return ReflectionHelper.GetAttributes<T>(provider, false);
        }

        public static IEnumerable<T> GetAttributes<T>(
                ICustomAttributeProvider provider,
                bool inherit)
            where T : Attribute
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            object[] attributes = provider.GetCustomAttributes(typeof(T), inherit);

            for (int index = 0; index < attributes.Length; index++)
                yield return (T)attributes[index];
        }

        public static bool IsDefined<T>(ICustomAttributeProvider provider)
        {
            return ReflectionHelper.IsDefined<T>(provider, false);
        }

        public static bool IsDefined<T>(ICustomAttributeProvider provider, bool inherit)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            return provider.IsDefined(typeof(T), inherit);
        }

        #endregion
    }
}

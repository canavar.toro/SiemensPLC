﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.Controls
{
    using System.Windows.Forms;

    public class DataGridViewNullImageCell : DataGridViewImageCell
    {
        #region ---------- Public constructors ----------

        public DataGridViewNullImageCell()
            : base()
        {
        }

        public DataGridViewNullImageCell(bool valueIsIcon)
            : base(valueIsIcon)
        {
        }

        #endregion

        #region ---------- Public properties ----------

        public override object DefaultNewRowValue
        {
            get
            {
                return null;
            }
        }

        #endregion
    }
}

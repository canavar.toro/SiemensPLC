﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    public partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label monitorLabel;
            System.Windows.Forms.Label projectLabel;
            System.Windows.Forms.ToolStrip projectToolStrip;
            System.Windows.Forms.StatusStrip statusStrip;
            System.Windows.Forms.SplitContainer splitContainer;
            System.Windows.Forms.TableLayoutPanel projectTableLayoutPanel;
            System.Windows.Forms.TableLayoutPanel monitorTableLayoutPanel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.ToolStrip monitorToolStrip;
            System.Windows.Forms.ErrorProvider errorProvider;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.projectPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.monitorNodesDataGridView = new System.Windows.Forms.DataGridView();
            this.statusDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dualValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hexValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monitorNodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.readFromPlcToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.writeToPlcToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.autoUpdateToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.showAboutToolStripButton = new System.Windows.Forms.ToolStripButton();
            monitorLabel = new System.Windows.Forms.Label();
            projectLabel = new System.Windows.Forms.Label();
            projectToolStrip = new System.Windows.Forms.ToolStrip();
            statusStrip = new System.Windows.Forms.StatusStrip();
            splitContainer = new System.Windows.Forms.SplitContainer();
            projectTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            monitorTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            monitorToolStrip = new System.Windows.Forms.ToolStrip();
            errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            projectToolStrip.SuspendLayout();
            statusStrip.SuspendLayout();
            splitContainer.Panel1.SuspendLayout();
            splitContainer.Panel2.SuspendLayout();
            splitContainer.SuspendLayout();
            projectTableLayoutPanel.SuspendLayout();
            monitorTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNodesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNodesBindingSource)).BeginInit();
            monitorToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // monitorLabel
            // 
            monitorLabel.AutoSize = true;
            monitorLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            monitorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            monitorLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            monitorLabel.Location = new System.Drawing.Point(0, 0);
            monitorLabel.Margin = new System.Windows.Forms.Padding(0);
            monitorLabel.MaximumSize = new System.Drawing.Size(0, 25);
            monitorLabel.MinimumSize = new System.Drawing.Size(0, 25);
            monitorLabel.Name = "monitorLabel";
            monitorLabel.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            monitorLabel.Size = new System.Drawing.Size(734, 25);
            monitorLabel.TabIndex = 4;
            monitorLabel.Text = "Monitor";
            monitorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectLabel
            // 
            projectLabel.AutoSize = true;
            projectLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            projectLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            projectLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            projectLabel.Location = new System.Drawing.Point(0, 0);
            projectLabel.Margin = new System.Windows.Forms.Padding(0);
            projectLabel.MaximumSize = new System.Drawing.Size(0, 25);
            projectLabel.MinimumSize = new System.Drawing.Size(0, 25);
            projectLabel.Name = "projectLabel";
            projectLabel.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            projectLabel.Size = new System.Drawing.Size(365, 25);
            projectLabel.TabIndex = 5;
            projectLabel.Text = "Project";
            projectLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // projectToolStrip
            // 
            projectToolStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            projectToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            projectToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton});
            projectToolStrip.Location = new System.Drawing.Point(0, 25);
            projectToolStrip.Name = "projectToolStrip";
            projectToolStrip.Size = new System.Drawing.Size(365, 25);
            projectToolStrip.TabIndex = 0;
            projectToolStrip.Text = "toolStrip";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::S7Watch.Resources.Open;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Open";
            this.openToolStripButton.Click += new System.EventHandler(this.HandleOpenToolStripButtonClick);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::S7Watch.Resources.Save;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.HandleSaveToolStripButtonClick);
            // 
            // statusStrip
            // 
            statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripStatusLabel});
            statusStrip.Location = new System.Drawing.Point(0, 419);
            statusStrip.Name = "statusStrip";
            statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            statusStrip.Size = new System.Drawing.Size(1112, 25);
            statusStrip.TabIndex = 2;
            statusStrip.Text = "statusStrip";
            // 
            // statusToolStripStatusLabel
            // 
            this.statusToolStripStatusLabel.Name = "statusToolStripStatusLabel";
            this.statusToolStripStatusLabel.Size = new System.Drawing.Size(50, 20);
            this.statusToolStripStatusLabel.Text = "Ready";
            // 
            // splitContainer
            // 
            splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer.Location = new System.Drawing.Point(0, 0);
            splitContainer.Margin = new System.Windows.Forms.Padding(4);
            splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            splitContainer.Panel1.Controls.Add(projectTableLayoutPanel);
            splitContainer.Panel1.Padding = new System.Windows.Forms.Padding(4, 4, 0, 4);
            // 
            // splitContainer.Panel2
            // 
            splitContainer.Panel2.Controls.Add(monitorTableLayoutPanel);
            splitContainer.Panel2.Padding = new System.Windows.Forms.Padding(0, 4, 4, 4);
            splitContainer.Size = new System.Drawing.Size(1112, 419);
            splitContainer.SplitterDistance = 369;
            splitContainer.SplitterWidth = 5;
            splitContainer.TabIndex = 3;
            // 
            // projectTableLayoutPanel
            // 
            projectTableLayoutPanel.ColumnCount = 1;
            projectTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            projectTableLayoutPanel.Controls.Add(projectLabel, 0, 0);
            projectTableLayoutPanel.Controls.Add(this.projectPropertyGrid, 0, 2);
            projectTableLayoutPanel.Controls.Add(projectToolStrip, 0, 1);
            projectTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            projectTableLayoutPanel.Location = new System.Drawing.Point(4, 4);
            projectTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            projectTableLayoutPanel.Name = "projectTableLayoutPanel";
            projectTableLayoutPanel.RowCount = 3;
            projectTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            projectTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            projectTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            projectTableLayoutPanel.Size = new System.Drawing.Size(365, 411);
            projectTableLayoutPanel.TabIndex = 0;
            // 
            // projectPropertyGrid
            // 
            this.projectPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectPropertyGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.projectPropertyGrid.Location = new System.Drawing.Point(0, 50);
            this.projectPropertyGrid.Margin = new System.Windows.Forms.Padding(0);
            this.projectPropertyGrid.Name = "projectPropertyGrid";
            this.projectPropertyGrid.Size = new System.Drawing.Size(365, 361);
            this.projectPropertyGrid.TabIndex = 6;
            // 
            // monitorTableLayoutPanel
            // 
            monitorTableLayoutPanel.ColumnCount = 1;
            monitorTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            monitorTableLayoutPanel.Controls.Add(this.monitorNodesDataGridView, 0, 2);
            monitorTableLayoutPanel.Controls.Add(monitorLabel, 0, 0);
            monitorTableLayoutPanel.Controls.Add(monitorToolStrip, 0, 1);
            monitorTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            monitorTableLayoutPanel.Location = new System.Drawing.Point(0, 4);
            monitorTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            monitorTableLayoutPanel.Name = "monitorTableLayoutPanel";
            monitorTableLayoutPanel.RowCount = 3;
            monitorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            monitorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            monitorTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            monitorTableLayoutPanel.Size = new System.Drawing.Size(734, 411);
            monitorTableLayoutPanel.TabIndex = 0;
            // 
            // monitorNodesDataGridView
            // 
            this.monitorNodesDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.monitorNodesDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.monitorNodesDataGridView.AutoGenerateColumns = false;
            this.monitorNodesDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.monitorNodesDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.monitorNodesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.monitorNodesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.statusDataGridViewImageColumn,
            this.addressDataGridViewTextBoxColumn,
            this.typeDataGridViewComboBoxColumn,
            this.valueDataGridViewTextBoxColumn,
            this.dualValueDataGridViewTextBoxColumn,
            this.hexValueDataGridViewTextBoxColumn});
            this.monitorNodesDataGridView.DataSource = this.monitorNodesBindingSource;
            this.monitorNodesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monitorNodesDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.monitorNodesDataGridView.Location = new System.Drawing.Point(0, 52);
            this.monitorNodesDataGridView.Margin = new System.Windows.Forms.Padding(0);
            this.monitorNodesDataGridView.Name = "monitorNodesDataGridView";
            this.monitorNodesDataGridView.Size = new System.Drawing.Size(734, 359);
            this.monitorNodesDataGridView.TabIndex = 6;
            this.monitorNodesDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.HandleDataGridViewCellBeginEdit);
            this.monitorNodesDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.HandleDataGridViewCellEndEdit);
            this.monitorNodesDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.HandleDataGridViewCellFormatting);
            // 
            // statusDataGridViewImageColumn
            // 
            this.statusDataGridViewImageColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.statusDataGridViewImageColumn.DataPropertyName = "StatusImage";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.statusDataGridViewImageColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.statusDataGridViewImageColumn.HeaderText = "";
            this.statusDataGridViewImageColumn.Image = global::S7Watch.Resources.Undefined;
            this.statusDataGridViewImageColumn.Name = "statusDataGridViewImageColumn";
            this.statusDataGridViewImageColumn.ReadOnly = true;
            this.statusDataGridViewImageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.statusDataGridViewImageColumn.Width = 21;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            // 
            // typeDataGridViewComboBoxColumn
            // 
            this.typeDataGridViewComboBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewComboBoxColumn.HeaderText = "Type";
            this.typeDataGridViewComboBoxColumn.Name = "typeDataGridViewComboBoxColumn";
            this.typeDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.typeDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.typeDataGridViewComboBoxColumn.Width = 120;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            // 
            // dualValueDataGridViewTextBoxColumn
            // 
            this.dualValueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.dualValueDataGridViewTextBoxColumn.HeaderText = "Bin";
            this.dualValueDataGridViewTextBoxColumn.Name = "dualValueDataGridViewTextBoxColumn";
            this.dualValueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hexValueDataGridViewTextBoxColumn
            // 
            this.hexValueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.hexValueDataGridViewTextBoxColumn.HeaderText = "Hex";
            this.hexValueDataGridViewTextBoxColumn.Name = "hexValueDataGridViewTextBoxColumn";
            this.hexValueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // monitorNodesBindingSource
            // 
            this.monitorNodesBindingSource.DataSource = typeof(S7Watch.WatchNode);
            // 
            // monitorToolStrip
            // 
            monitorToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            monitorToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromPlcToolStripButton,
            this.writeToPlcToolStripButton,
            this.toolStripSeparator1,
            this.autoUpdateToolStripButton,
            this.showAboutToolStripButton});
            monitorToolStrip.Location = new System.Drawing.Point(0, 25);
            monitorToolStrip.Name = "monitorToolStrip";
            monitorToolStrip.Size = new System.Drawing.Size(734, 27);
            monitorToolStrip.TabIndex = 5;
            monitorToolStrip.Text = "toolStrip";
            // 
            // readFromPlcToolStripButton
            // 
            this.readFromPlcToolStripButton.Image = global::S7Watch.Resources.ReadFromPlc;
            this.readFromPlcToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.readFromPlcToolStripButton.Name = "readFromPlcToolStripButton";
            this.readFromPlcToolStripButton.Size = new System.Drawing.Size(126, 24);
            this.readFromPlcToolStripButton.Text = "Read from PLC";
            this.readFromPlcToolStripButton.Click += new System.EventHandler(this.HandleReadFromPlcToolStripButtonClick);
            // 
            // writeToPlcToolStripButton
            // 
            this.writeToPlcToolStripButton.Image = global::S7Watch.Resources.WriteToPlc;
            this.writeToPlcToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.writeToPlcToolStripButton.Name = "writeToPlcToolStripButton";
            this.writeToPlcToolStripButton.Size = new System.Drawing.Size(110, 24);
            this.writeToPlcToolStripButton.Text = "Write to PLC";
            this.writeToPlcToolStripButton.Click += new System.EventHandler(this.HandleWriteToPlcToolStripButtonClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // autoUpdateToolStripButton
            // 
            this.autoUpdateToolStripButton.CheckOnClick = true;
            this.autoUpdateToolStripButton.Image = global::S7Watch.Resources.AutoUpdate;
            this.autoUpdateToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.autoUpdateToolStripButton.Name = "autoUpdateToolStripButton";
            this.autoUpdateToolStripButton.Size = new System.Drawing.Size(114, 24);
            this.autoUpdateToolStripButton.Text = "Auto Update";
            this.autoUpdateToolStripButton.ToolTipText = "Auto Update (every 1s)";
            this.autoUpdateToolStripButton.CheckedChanged += new System.EventHandler(this.HandleAutoUpdateToolStripButtonCheckedChanged);
            // 
            // showAboutToolStripButton
            // 
            this.showAboutToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.showAboutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showAboutToolStripButton.Image = global::S7Watch.Resources.About;
            this.showAboutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showAboutToolStripButton.Name = "showAboutToolStripButton";
            this.showAboutToolStripButton.Size = new System.Drawing.Size(23, 24);
            this.showAboutToolStripButton.Text = "About";
            this.showAboutToolStripButton.Click += new System.EventHandler(this.HandleShowAboutToolStripButtonClick);
            // 
            // errorProvider
            // 
            errorProvider.ContainerControl = this;
            errorProvider.DataSource = this.monitorNodesBindingSource;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 444);
            this.Controls.Add(splitContainer);
            this.Controls.Add(statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainView";
            projectToolStrip.ResumeLayout(false);
            projectToolStrip.PerformLayout();
            statusStrip.ResumeLayout(false);
            statusStrip.PerformLayout();
            splitContainer.Panel1.ResumeLayout(false);
            splitContainer.Panel2.ResumeLayout(false);
            splitContainer.ResumeLayout(false);
            projectTableLayoutPanel.ResumeLayout(false);
            projectTableLayoutPanel.PerformLayout();
            monitorTableLayoutPanel.ResumeLayout(false);
            monitorTableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNodesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNodesBindingSource)).EndInit();
            monitorToolStrip.ResumeLayout(false);
            monitorToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PropertyGrid projectPropertyGrid;
        private System.Windows.Forms.DataGridView monitorNodesDataGridView;
        private System.Windows.Forms.BindingSource monitorNodesBindingSource;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton writeToPlcToolStripButton;
        private System.Windows.Forms.ToolStripButton autoUpdateToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel statusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripButton readFromPlcToolStripButton;
        private System.Windows.Forms.ToolStripButton showAboutToolStripButton;
        private System.Windows.Forms.DataGridViewImageColumn statusDataGridViewImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn typeDataGridViewComboBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dualValueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hexValueDataGridViewTextBoxColumn;
    }
}
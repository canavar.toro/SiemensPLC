﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.ComponentModel;

    public class EnumBindingList<T> : BindingList<EnumMember<T>>
        where T : new()
    {
        #region ---------- Public constructors ----------

        public EnumBindingList()
            : base()
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException();

            foreach (var member in new Enum<T>().Members)
                this.Add(new EnumMember<T>(member.Field));
        }

        #endregion
    }
}

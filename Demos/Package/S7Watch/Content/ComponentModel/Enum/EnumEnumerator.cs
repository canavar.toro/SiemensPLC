﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    internal class EnumEnumerator<T> : IEnumerator<T>
        where T : new()
    {
        #region ---------- Private fields ----------

        private T current;
        private Enum<T> enumObject;
        private int index;

        #endregion

        #region ---------- Public constructors ----------

        public EnumEnumerator(Enum<T> enumObject)
            : base()
        {
            if (enumObject == null)
                throw new ArgumentNullException("enumObject");

            this.enumObject = enumObject;
            this.index = -1;
        }

        #endregion

        #region ---------- Public properties ----------

        public T Current
        {
            get
            {
                return this.current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return this.current;
            }
        }

        public Enum<T> Enum
        {
            get
            {
                return this.enumObject;
            }
        }

        #endregion

        #region ---------- Public methods ----------

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (++this.index >= this.enumObject.Members.Count)
                return false;

            this.current = this.enumObject.Members[this.index].Value;
            return true;
        }

        public void Reset()
        {
            this.index = -1;
        }

        #endregion
    }
}

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class EnumMemberNameAttribute : Attribute
    {
        #region ---------- Private fields ----------

        private string name;

        #endregion

        #region ---------- Public constructors ----------

        public EnumMemberNameAttribute(string name)
            : base()
        {
            this.name = name;
        }

        #endregion

        #region ---------- Public properties ----------

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        #endregion
    }
}

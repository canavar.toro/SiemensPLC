﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Reflection;

    public class EnumMemberCollection<T> : KeyedCollection<T, EnumMember<T>>
        where T : new()
    {
        #region ---------- Internal constructors ----------

        internal EnumMemberCollection()
            : base()
        {
        }

        #endregion

        #region ---------- Internal methods ----------

        internal EnumMember<T> Add(FieldInfo field)
        {
            var item = new EnumMember<T>(field);
            this.Add(item);

            return item;
        }

        #endregion

        #region ---------- Protected methods ----------

        protected override T GetKeyForItem(EnumMember<T> item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return item.Value;
        }

        #endregion
    }
}

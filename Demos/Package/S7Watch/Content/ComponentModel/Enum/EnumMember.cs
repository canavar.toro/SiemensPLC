﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.Diagnostics;
    using System.Reflection;

    [DebuggerDisplay("{Name}")]
    public class EnumMember<T>
        where T : new()
    {
        #region ---------- Private fields ----------

        private FieldInfo field;
        private string name;
        private T value;

        #endregion

        #region ---------- Internal constructors ----------

        internal EnumMember(FieldInfo field)
            : base()
        {
            if (field == null)
                throw new ArgumentNullException("field");

            if (!typeof(T).IsEnum)
                throw new ArgumentException();

            this.field = field;
            this.value = (T)field.GetValue(new T());

            var nameAttribute = ReflectionHelper.GetAttribute<EnumMemberNameAttribute>(field);

            if (nameAttribute != null) {
                this.name = nameAttribute.Name.Trim();

                if (this.name.Length == 0)
                    this.name = null;
            }

            this.name = this.name ?? field.Name;
        }

        #endregion

        #region ---------- Public properties ----------

        public FieldInfo Field
        {
            get
            {
                return this.field;
            }
        }

        [Obfuscation(StripAfterObfuscation = true)]
        public virtual string FullName
        {
            get
            {
                return typeof(T).Name + "." + this.Name;
            }
        }

        [Obfuscation(StripAfterObfuscation = true)]
        public virtual string Name
        {
            get
            {
                return this.name;
            }
        }

        [Obfuscation(StripAfterObfuscation = true)]
        public T Value
        {
            get
            {
                return this.value;
            }
        }

        #endregion

        #region ---------- Public static methods ----------

        public static EnumMember<T> Of(T value)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException();

            foreach (var field in typeof(T).GetFields()) {
                if (field.FieldType.IsEnum && Object.Equals((T)field.GetValue(new T()), value))
                    return new EnumMember<T>(field);
            }

            throw new ArgumentException();
        }

        #endregion
    }
}

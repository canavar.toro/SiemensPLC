﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;

    [DebuggerDisplay("{Name}")]
    public class Enum<T> : IEnumerable<T>
        where T : new()
    {
        #region ---------- Private fields ----------

        private EnumMemberCollection<T> members;

        #endregion

        #region ---------- Public constructors ----------

        public Enum()
            : base()
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException();

            this.members = new EnumMemberCollection<T>();
            var fields = new List<FieldInfo>();

            foreach (var field in typeof(T).GetFields()) {
                if (field.FieldType.IsEnum)
                    this.members.Add(field);
            }
        }

        #endregion

        #region ---------- Public properties ----------

        public EnumMemberCollection<T> Members
        {
            get
            {
                return this.members;
            }
        }

        public string Name
        {
            get
            {
                return typeof(T).Name;
            }
        }

        #endregion

        #region ---------- Public methods ----------

        public IEnumerator<T> GetEnumerator()
        {
            return new EnumEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}

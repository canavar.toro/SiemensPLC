﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;

    public class EnumConverter<T> : StringConverter
        where T : new()
    {
        #region ---------- Private fields ----------

        private Dictionary<string, T> nameValuePairs;
        private Dictionary<T, string> valueNamePairs;

        #endregion

        #region ---------- Public constructors ----------

        public EnumConverter()
            : base()
        {
            this.nameValuePairs = new Dictionary<string, T>();
            this.valueNamePairs = new Dictionary<T, string>();

            foreach (var member in new Enum<T>().Members) {
                this.nameValuePairs.Add(member.Name, member.Value);
                this.valueNamePairs.Add(member.Value, member.Name);
            }
        }

        #endregion

        #region ---------- Public methods ----------

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(
                ITypeDescriptorContext context,
                CultureInfo culture,
                object value)
        {
            var key = (string)value;

            if (this.nameValuePairs.ContainsKey(key))
                return this.nameValuePairs[key];

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(
                ITypeDescriptorContext context,
                CultureInfo culture,
                object value,
                Type destinationType)
        {
            if (destinationType == typeof(string)) {
                if (value is string)
                    return (string)value;

                var key = (T)value;

                if (this.valueNamePairs.ContainsKey(key))
                    return this.valueNamePairs[key];
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(this.nameValuePairs.Keys);
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            return this.nameValuePairs.ContainsKey(value as string);
        }

        #endregion
    }
}

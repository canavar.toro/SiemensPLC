﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Net;

    public class IPAddressConverter : TypeConverter
    {
        #region ---------- Public constructors ----------

        public IPAddressConverter()
            : base()
        {
        }

        #endregion

        #region ---------- Public methods ----------

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(
                ITypeDescriptorContext context,
                CultureInfo culture,
                object value)
        {
            var stringValue = value as string;

            if (stringValue == null)
                return base.ConvertFrom(context, culture, value);

            return IPAddress.Parse(stringValue);
        }

        public override object ConvertTo(
                ITypeDescriptorContext context,
                CultureInfo culture,
                object value,
                Type destinationType)
        {
            var address = value as IPAddress;

            if (destinationType == typeof(string)) {
                if (address == null)
                    return null;

                return address.ToString();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            var stringValue = value as string;

            IPAddress address = null;
            return IPAddress.TryParse(stringValue, out address);
        }

        #endregion
    }
}

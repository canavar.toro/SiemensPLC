﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch.ComponentModel
{
    using System;
    using System.ComponentModel;

    public abstract class Observable : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region ---------- Private fields ----------

        private ISynchronizeInvoke synchronizeInvoke;

        #endregion

        #region ---------- Protected constructors ----------

        protected Observable()
            : base()
        {
        }

        #endregion

        #region ---------- Public events ----------

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;
        public event EventHandler SynchronizeInvokeChanged;

        #endregion

        #region ---------- Public properties ----------

        [Browsable(false)]
        public ISynchronizeInvoke SynchronizeInvoke
        {
            get
            {
                return this.synchronizeInvoke;
            }

            set
            {
                if (this.synchronizeInvoke != value) {
                    this.synchronizeInvoke = value;
                    this.OnSynchronizeInvokeChanged(EventArgs.Empty);
                }
            }
        }

        #endregion

        #region ---------- Protected methods ----------

        protected void InvokeSynchronized(Action method)
        {
            if (this.synchronizeInvoke == null || !this.synchronizeInvoke.InvokeRequired)
                method();
            else
                this.synchronizeInvoke.Invoke(method, null);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
                this.InvokeSynchronized(() => handler(this, e));
        }

        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            var handler = this.PropertyChanging;

            if (handler != null)
                this.InvokeSynchronized(() => handler(this, e));
        }

        protected virtual void OnSynchronizeInvokeChanged(EventArgs e)
        {
            var handler = this.SynchronizeInvokeChanged;

            if (handler != null)
                this.InvokeSynchronized(() => handler(this, e));
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected void RaisePropertyChanging(string propertyName)
        {
            this.OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        #endregion
    }
}

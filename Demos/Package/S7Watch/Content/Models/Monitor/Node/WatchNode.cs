﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Text;

    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public class WatchNode : Observable
    {
        #region ---------- Private fields ----------

        private WatchEntry entry;
        private bool hasValueChanged;
        private bool isEditing;
        private object value;
        private IPlcValue valueBuffer;

        #endregion

        #region ---------- Public constructors ----------

        public WatchNode()
            : this(new WatchEntry())
        {
        }

        public WatchNode(WatchEntry entry)
            : base()
        {
            this.entry = entry;
            this.entry.PropertyChanged += this.HandleEntryPropertyChanged;

            this.UpdateValueBuffer();
        }

        #endregion

        #region ---------- Public properties ----------

        public PlcAddress Address
        {
            get
            {
                return this.entry.Address;
            }

            set
            {
                this.entry.Address = value;
            }
        }

        [Browsable(false)]
        public WatchEntry Entry
        {
            get
            {
                return this.entry;
            }
        }

        [Browsable(false)]
        public bool HasValueChanged
        {
            get
            {
                return this.hasValueChanged;
            }
        }

        public bool IsEditing
        {
            get
            {
                return this.isEditing;
            }
        }

        public WatchEntryType Type
        {
            get
            {
                return this.entry.Type;
            }

            set
            {
                this.entry.Type = value;
            }
        }

        public PlcStatus Status
        {
            get
            {
                return this.valueBuffer.Status;
            }
        }

        public int StatusCode
        {
            get
            {
                return (int)this.valueBuffer.Status.Code;
            }
        }

        public Image StatusImage
        {
            get
            {
                var status = this.valueBuffer.Status;

                if (status.Code == PlcStatusCode.NoError)
                    return Resources.Success;

                return Resources.Error;
            }
        }

        public string StatusText
        {
            get
            {
                return this.valueBuffer.Status.Text;
            }
        }

        public object Value
        {
            get
            {
                return this.value;
            }

            set
            {
                if (this.value != value) {
                    var canChange = false;
                    var type = this.entry.Type;

                    if (type == WatchEntryType.Bool) {
                        var result = false;
                        canChange = Boolean.TryParse((string)value, out result);

                        if (!canChange) {
                            var numericResult = 0;
                            canChange = Int32.TryParse((string)value, out numericResult);

                            if (canChange)
                                result = numericResult != 0;
                        }

                        if (canChange)
                            value = result;
                    }
                    if (type == WatchEntryType.Time || type == WatchEntryType.TimeOfDay) {
                        var result = TimeSpan.MinValue;
                        canChange = TimeSpan.TryParse((string)value, out result);

                        if (canChange)
                            value = result;
                    }
                    else if (type == WatchEntryType.Date || type == WatchEntryType.DateTime) {
                        var result = DateTime.MinValue;
                        canChange = DateTime.TryParse((string)value, out result);

                        if (canChange)
                            value = result;
                    }
                    else {
                        try {
                            value = Convert.ChangeType(
                                    value, this.valueBuffer.Type.FrameworkType);

                            canChange = true;
                        }
                        catch (FormatException) {
                            canChange = false;
                        }
                    }

                    if (canChange) {
                        this.value = value;
                        this.hasValueChanged = true;

                        this.RaisePropertyChanged("Value");
                    }
                }
            }
        }

        [Browsable(false)]
        public IPlcValue ValueBuffer
        {
            get
            {
                return this.valueBuffer;
            }
        }

        #endregion

        #region ---------- Public methods ----------

        public void AcceptValueChanges()
        {
            this.valueBuffer.Value = this.value;
            this.hasValueChanged = false;
        }

        public void BeginEdit()
        {
            this.isEditing = true;
        }

        public void EndEdit()
        {
            this.isEditing = false;
        }

        public byte[] GetValueBytes()
        {
            var type = this.entry.Type;
            var value = this.value;

            if (type == WatchEntryType.Bool)
                return BitConverter.GetBytes((bool)value);
            if (type == WatchEntryType.Byte)
                return new byte[] { (byte)value };
            if (type == WatchEntryType.Char)
                return BitConverter.GetBytes((char)value);
            if (type == WatchEntryType.DInt)
                return BitConverter.GetBytes((int)value);
            if (type == WatchEntryType.Int)
                return BitConverter.GetBytes((short)value);
            if (type == WatchEntryType.DWord)
                return BitConverter.GetBytes((uint)value);
            if (type == WatchEntryType.Word)
                return BitConverter.GetBytes((ushort)value);
            if (type == WatchEntryType.Real)
                return BitConverter.GetBytes((float)value);
            if (type == WatchEntryType.Double)
                return BitConverter.GetBytes((double)value);
            if (type == WatchEntryType.String)
                return Encoding.ASCII.GetBytes((string)value);
            if (type == WatchEntryType.Time || type == WatchEntryType.TimeOfDay)
                return BitConverter.GetBytes(((TimeSpan)value).TotalMilliseconds);
            if (type == WatchEntryType.Date || type == WatchEntryType.DateTime)
                return BitConverter.GetBytes(((DateTime)value).ToBinary());

            return new byte[] { };
        }

        public void RejectValueChanges()
        {
            this.value = this.valueBuffer.Value;
            this.hasValueChanged = false;

            this.RaisePropertyChanged("Value");
        }

        #endregion

        #region ---------- Private methods ----------

        private void HandleEntryPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Address")
                this.RaisePropertyChanged("Address");
            else if (e.PropertyName == "Type")
                this.RaisePropertyChanged("Type");

            if (e.PropertyName == "Address" || e.PropertyName == "Type")
                this.UpdateValueBuffer();
        }

        private void HandleValueBufferChanged(object sender, ValueChangedEventArgs e)
        {
            if (!this.hasValueChanged)
                this.RejectValueChanges();
        }

        private void HandleValueBufferStatusChanged(object sender, EventArgs e)
        {
            this.RaisePropertyChanged("Status");
            this.RaisePropertyChanged("StatusCode");
            this.RaisePropertyChanged("StatusImage");
            this.RaisePropertyChanged("StatusText");
        }

        private void UpdateValueBuffer()
        {
            var address = this.entry.Address;
            var type = this.entry.Type;

            var newValueBuffer = this.valueBuffer;

            if (type == WatchEntryType.Bool)
                newValueBuffer = new PlcBoolean(address);
            else if (type == WatchEntryType.Byte)
                newValueBuffer = new PlcByte(address);
            else if (type == WatchEntryType.Char)
                newValueBuffer = new PlcChar(address);
            else if (type == WatchEntryType.Date)
                newValueBuffer = new PlcDate(address);
            else if (type == WatchEntryType.DateTime)
                newValueBuffer = new PlcDateTime(address);
            else if (type == WatchEntryType.DInt)
                newValueBuffer = new PlcInt32(address);
            else if (type == WatchEntryType.DWord)
                newValueBuffer = new PlcUInt32(address);
            else if (type == WatchEntryType.Int)
                newValueBuffer = new PlcInt16(address);
            else if (type == WatchEntryType.Word)
                newValueBuffer = new PlcUInt16(address);
            else if (type == WatchEntryType.Real)
                newValueBuffer = new PlcReal(address);
            else if (type == WatchEntryType.Double)
                newValueBuffer = new PlcDouble(address);
            else if (type == WatchEntryType.String)
                newValueBuffer = new PlcString(address, 16);
            else if (type == WatchEntryType.Time)
                newValueBuffer = new PlcTime(address);
            else if (type == WatchEntryType.TimeOfDay)
                newValueBuffer = new PlcTimeOfDay(address);

            if (this.valueBuffer != newValueBuffer) {
                if (this.valueBuffer != null) {
                    this.valueBuffer.Changed -= this.HandleValueBufferChanged;
                    this.valueBuffer.Status.Changed -= this.HandleValueBufferStatusChanged;
                }

                this.valueBuffer = newValueBuffer;

                if (this.valueBuffer != null) {
                    this.valueBuffer.Changed += this.HandleValueBufferChanged;
                    this.valueBuffer.Status.Changed += this.HandleValueBufferStatusChanged;
                }

                this.RejectValueChanges();
            }
        }

        #endregion
    }
}

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;

    public class WatchNodeCollection : BindingList<WatchNode>
    {
        #region ---------- Private fields ----------

        private WatchMonitor owner;

        #endregion

        #region ---------- Public constructors ----------

        public WatchNodeCollection(WatchMonitor owner)
            : base()
        {
            this.owner = owner;
            this.owner.SynchronizeInvokeChanged += this.HandleOwnerSynchronizeInvokeChanged;
        }

        #endregion

        #region ---------- Protected methods ----------

        protected override void OnListChanged(ListChangedEventArgs e)
        {
            this.LinkSynchronizeInvoke();
            base.OnListChanged(e);
        }

        #endregion

        #region ---------- Private methods ----------

        private void HandleOwnerSynchronizeInvokeChanged(object sender, EventArgs e)
        {
            this.LinkSynchronizeInvoke();
        }

        private void LinkSynchronizeInvoke()
        {
            for (int index = 0; index < this.Count; index++)
                this.Items[index].SynchronizeInvoke = this.owner.SynchronizeInvoke;
        }

        #endregion
    }
}

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Threading;

    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public class WatchMonitor : Observable
    {
        #region ---------- Private static fields ----------

        private static readonly object SyncRoot = new object();

        #endregion

        #region ---------- Private fields ----------

        private PlcDeviceConnection connection;
        private IPlcDevice device;
        private bool isEnabled;
        private WatchProject project;
        private WatchNodeCollection nodes;
        private Timer timer;

        #endregion

        #region ---------- Public constructors ----------

        public WatchMonitor(WatchProject project)
            : base()
        {
            this.project = project;
            this.project.PropertyChanged += this.HandlePropertyChanged;

            this.nodes = new WatchNodeCollection(this);

            foreach (var entry in this.project.Entries)
                this.nodes.Add(new WatchNode(entry));

            this.nodes.ListChanged += this.HandleNodesListChanged;
            this.timer = new Timer(this.HandleTimerElapsed);

            this.UpdateDriverObjects();
        }

        #endregion

        #region ---------- Public properties ----------

        public bool IsEnabled
        {
            get
            {
                return this.isEnabled;
            }

            set
            {
                if (this.isEnabled != value) {
                    if (value)
                        this.Start();
                    else
                        this.Stop();

                    if (this.isEnabled == value)
                        this.RaisePropertyChanged("IsEnabled");
                }
            }
        }

        public WatchNodeCollection Nodes
        {
            get
            {
                return this.nodes;
            }
        }

        public WatchProject Project
        {
            get
            {
                return this.project;
            }
        }

        #endregion

        #region ---------- Public methods ----------

        public void ReadValues()
        {
            this.Invoke(() => {
                foreach (var node in this.nodes)
                    node.RejectValueChanges();

                var values = this.nodes.Select((node) => node.ValueBuffer);
                this.connection.ReadValues(values);
            });
        }

        public void Start()
        {
            if (!this.isEnabled) {
                lock (WatchMonitor.SyncRoot) {
                    this.timer.Change(this.project.Interval, this.project.Interval);
                    this.isEnabled = true;
                }
            }
        }

        public void Stop()
        {
            if (this.isEnabled) {
                lock (WatchMonitor.SyncRoot) {
                    this.timer.Change(Timeout.Infinite, Timeout.Infinite);
                    this.isEnabled = false;
                }
            }
        }

        public void Synchronize()
        {
            this.Invoke(() => {
                var nodeGroups = from node in this.nodes
                                 where !node.IsEditing
                                 group node by node.HasValueChanged;

                foreach (var nodeGroup in nodeGroups) {
                    var values = nodeGroup.Select((node) => node.ValueBuffer);

                    if (nodeGroup.Key) {
                        foreach (var node in nodeGroup)
                            node.AcceptValueChanges();

                        this.connection.WriteValues(values);
                    }

                    this.connection.ReadValues(values);
                }
            });
        }

        public void WriteValues()
        {
            this.Invoke(() => {
                foreach (var node in this.nodes)
                    node.AcceptValueChanges();

                var values = this.nodes.Select((node) => node.ValueBuffer);
                this.connection.WriteValues(values);
            });
        }

        #endregion

        #region ---------- Private methods ----------

        private void HandleNodesListChanged(object sender, ListChangedEventArgs e)
        {
            var entries = this.project.Entries;
            entries.Clear();

            foreach (var node in this.nodes)
                entries.Add(node.Entry);
        }

        private void HandlePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.Invoke(this.UpdateDriverObjects);
        }

        private void HandleTimerElapsed(object state)
        {
            this.Synchronize();
        }

        private void Invoke(Action method)
        {
            bool wasEnabled = this.isEnabled;

            try {
                this.Stop();
                this.InvokeSynchronized(method);
            }
            catch {
            }

            if (wasEnabled)
                this.Start();
        }

        private void UpdateDriverObjects()
        {
            if (this.connection != null)
                this.connection.Close();

            this.device = this.project.Device.CreateDevice();
            this.connection = this.device.CreateConnection();

            if (this.connection != null)
                this.connection.Open();
        }

        #endregion
    }
}

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using S7Watch.ComponentModel;

    public enum WatchDeviceProvider : int
    {
        [EnumMemberName("Siemens")]
        Siemens = 0,

        [EnumMemberName("Virtual")]
        Virtual = 1
    }
}

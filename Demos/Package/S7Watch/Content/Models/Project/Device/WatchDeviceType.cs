﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public enum WatchDeviceType : int
    {
        [EnumMemberName("LOGO!")]
        Logo = SiemensDeviceType.Logo,

        [EnumMemberName("S7-200")]
        S7200 = SiemensDeviceType.S7200,

        [EnumMemberName("S7-300/400")]
        S7300_400 = SiemensDeviceType.S7300_400,

        [EnumMemberName("S7-1200")]
        S71200 = SiemensDeviceType.S71200,

        [EnumMemberName("S7-1500")]
        S71500 = SiemensDeviceType.S71500
    }
}

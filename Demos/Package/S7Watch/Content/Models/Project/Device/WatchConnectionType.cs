﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using S7Watch.ComponentModel;

    public enum WatchConnectionType : int
    {
        [EnumMemberName("Operation Panel")]
        OperationPanel = 0,

        [EnumMemberName("Programmer Device")]
        ProgrammerDevice = 1,

        [EnumMemberName("Other")]
        Other = 2
    }
}

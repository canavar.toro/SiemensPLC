﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Xml.Linq;

    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public class WatchDevice : Observable
    {
        #region ---------- Public static fields ----------

        public static readonly string ElementName = "Device";

        #endregion

        #region ---------- Private fields ----------

        private WatchConnectionType channel;
        private WatchDeviceEndPoint endPoint;
        private WatchDeviceProvider provider;
        private WatchDeviceType type;

        #endregion

        #region ---------- Public constructors ----------

        public WatchDevice()
            : base()
        {
            this.EndPoint = new WatchDeviceEndPoint();

            this.channel = WatchConnectionType.Other;
            this.provider = WatchDeviceProvider.Siemens;
            this.type = WatchDeviceType.S7300_400;
        }

        #endregion

        #region ---------- Public properties ----------

        [NotifyParentProperty(true)]
        [Category("Device")]
        [DefaultValue(WatchConnectionType.Other)]
        [TypeConverter(typeof(EnumConverter<WatchConnectionType>))]
        [DisplayName("Channel")]
        [Description("The channel of the PLC connection.")]
        public WatchConnectionType Channel
        {
            get
            {
                return this.channel;
            }

            set
            {
                if (this.channel != value) {
                    this.channel = value;
                    this.RaisePropertyChanged("Channel");
                }
            }
        }

        [NotifyParentProperty(true)]
        [Category("Device")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("End Point")]
        [Description("The end point information of the PLC device.")]
        public WatchDeviceEndPoint EndPoint
        {
            get
            {
                return this.endPoint;
            }

            private set
            {
                if (this.endPoint != value) {
                    if (this.endPoint != null)
                        this.endPoint.PropertyChanged -= this.HandleEndPointPropertyChanged;

                    this.endPoint = value;

                    if (this.endPoint != null)
                        this.endPoint.PropertyChanged += this.HandleEndPointPropertyChanged;

                    this.RaisePropertyChanged("EndPoint");
                }
            }
        }

        [NotifyParentProperty(true)]
        [Category("Device")]
#if DEBUG
        [DefaultValue(WatchDeviceProvider.Virtual)]
#else
        [DefaultValue(WatchDeviceProvider.Siemens)]
#endif
        [TypeConverter(typeof(EnumConverter<WatchDeviceProvider>))]
        [DisplayName("Provider")]
        [Description("The provider of the PLC device.")]
        public WatchDeviceProvider Provider
        {
            get
            {
                return this.provider;
            }

            set
            {
                if (this.provider != value) {
                    this.provider = value;
                    this.RaisePropertyChanged("Provider");
                }
            }
        }

        [NotifyParentProperty(true)]
        [Category("Device")]
        [DefaultValue(WatchDeviceType.S7300_400)]
        [TypeConverter(typeof(EnumConverter<WatchDeviceType>))]
        [DisplayName("Type")]
        [Description("The type of PLC device.")]
        public WatchDeviceType Type
        {
            get
            {
                return this.type;
            }

            set
            {
                if (this.type != value) {
                    this.type = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }

        #endregion

        #region ---------- Public static methods ----------

        public static WatchDevice FromXElement(XElement element)
        {
            var device = new WatchDevice();

            var channelAttribute = element.Attribute("Channel");

            if (channelAttribute != null) {
                device.Channel = (WatchConnectionType)Enum.Parse(
                        typeof(WatchConnectionType), channelAttribute.Value);
            }

            var providerAttribute = element.Attribute("Provider");

            if (providerAttribute != null) {
                device.Provider = (WatchDeviceProvider)Enum.Parse(
                        typeof(WatchDeviceProvider), providerAttribute.Value);
            }

            var typeAttribute = element.Attribute("Type");

            if (typeAttribute != null) {
                device.Type = (WatchDeviceType)Enum.Parse(
                        typeof(WatchDeviceType), typeAttribute.Value);
            }

            var endPointElement = element.Element(WatchDeviceEndPoint.ElementName);

            if (endPointElement != null)
                device.EndPoint = WatchDeviceEndPoint.FromXElement(endPointElement);

            return device;
        }

        #endregion

        #region ---------- Public methods ----------

        public IPlcDevice CreateDevice()
        {
            IPlcDevice device = null;
            var endPoint = this.endPoint.CreateEndPoint();

            if (this.provider == WatchDeviceProvider.Siemens) {
                var siemensDevice = new SiemensDevice(endPoint);
                siemensDevice.Type = (SiemensDeviceType)(int)this.type;

                device = siemensDevice;
            }
            else if (this.provider == WatchDeviceProvider.Virtual) {
                var virtualDevice = new VirtualDevice(endPoint);
                device = virtualDevice;
            }

            return device;
        }

        public override string ToString()
        {
            return string.Format(
                    "{0} {1} ({2})",
                    EnumMember<WatchDeviceProvider>.Of(this.provider).Name,
                    EnumMember<WatchDeviceType>.Of(this.type).Name,
                    EnumMember<WatchConnectionType>.Of(this.Channel).Name);
        }

        public XElement ToXElement()
        {
            return new XElement(
                    WatchDevice.ElementName,
                    new XAttribute("Channel", this.channel),
                    new XAttribute("Provider", this.provider),
                    new XAttribute("Type", this.type),
                    this.endPoint.ToXElement());
        }

        #endregion

        #region ---------- Private methods ----------

        private void HandleEndPointPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.RaisePropertyChanged(WatchDeviceEndPoint.ElementName + "." + e.PropertyName);
        }

        #endregion
    }
}

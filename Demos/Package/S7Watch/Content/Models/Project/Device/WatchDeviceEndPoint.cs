﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Net;
    using System.Xml.Linq;

    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public class WatchDeviceEndPoint : Observable
    {
        #region ---------- Public static fields ----------

        public static readonly string ElementName = "EndPoint";

        #endregion

        #region ---------- Private fields ----------

        private IPAddress address;
        private int rack;
        private int slot;

        #endregion

        #region ---------- Public constructors ----------

        public WatchDeviceEndPoint()
            : base()
        {
            this.address = IPAddress.Any;
            this.rack = IPDeviceEndPoint.DefaultRack;
            this.slot = IPDeviceEndPoint.DefaultSlot;
        }

        #endregion

        #region ---------- Public properties ----------

        [NotifyParentProperty(true)]
        [Category("General")]
        [DefaultValue("0.0.0.0")]
        [TypeConverter(typeof(IPAddressConverter))]
        [DisplayName("Address")]
        [Description("The IP address of the PLC device its data is to be watched.")]
        public IPAddress Address
        {
            get
            {
                return this.address;
            }

            set
            {
                if (this.address != value) {
                    this.address = value;
                    this.RaisePropertyChanged("Address");
                }
            }
        }

        [NotifyParentProperty(true)]
        [Category("Location")]
        [DefaultValue(IPDeviceEndPoint.DefaultRack)]
        [DisplayName("Rack")]
        [Description("The rack number of the PLC device.")]
        public int Rack
        {
            get
            {
                return this.rack;
            }

            set
            {
                if (value < IPDeviceEndPoint.MinRack || value > IPDeviceEndPoint.MaxRack)
                    value = IPDeviceEndPoint.DefaultRack;

                if (this.rack != value) {
                    this.rack = value;
                    this.RaisePropertyChanged("Rack");
                }
            }
        }

        [NotifyParentProperty(true)]
        [Category("Location")]
        [DefaultValue(IPDeviceEndPoint.DefaultSlot)]
        [DisplayName("Slot")]
        [Description("The slot number of the PLC device.")]
        public int Slot
        {
            get
            {
                return this.slot;
            }

            set
            {
                if (value < IPDeviceEndPoint.MinSlot || value > IPDeviceEndPoint.MaxSlot)
                    value = IPDeviceEndPoint.DefaultSlot;

                if (this.slot != value) {
                    this.slot = value;
                    this.RaisePropertyChanged("Slot");
                }
            }
        }

        #endregion

        #region ---------- Public static methods ----------

        public static WatchDeviceEndPoint FromXElement(XElement element)
        {
            var endPoint = new WatchDeviceEndPoint();
            var addressAttribute = element.Attribute("Address");

            if (addressAttribute != null)
                endPoint.Address = IPAddress.Parse(addressAttribute.Value);

            var rackAttribute = element.Attribute("Rack");

            if (rackAttribute != null)
                endPoint.Rack = Convert.ToInt32(rackAttribute.Value);

            var slotAttribute = element.Attribute("Slot");

            if (slotAttribute != null)
                endPoint.Slot = Convert.ToInt32(slotAttribute.Value);

            return endPoint;
        }

        #endregion

        #region ---------- Public methods ----------

        public PlcDeviceEndPoint CreateEndPoint()
        {
            return new IPDeviceEndPoint(this.address, this.rack, this.slot);
        }

        public override string ToString()
        {
            ////return StringHelper.Format(
            ////        "{0}, Rack={1}, Slot={2}",
            ////        this.address,
            ////        this.rack,
            ////        this.slot);
            return null;
        }

        public XElement ToXElement()
        {
            return new XElement(
                    WatchDeviceEndPoint.ElementName,
                    new XAttribute("Address", this.address),
                    new XAttribute("Rack", this.rack),
                    new XAttribute("Slot", this.slot));
        }

        #endregion
    }
}

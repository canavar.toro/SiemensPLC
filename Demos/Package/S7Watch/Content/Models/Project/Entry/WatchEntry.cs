﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Xml.Linq;

    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public class WatchEntry : Observable
    {
        #region ---------- Public static fields ----------

        public static readonly string ElementName = "Entry";

        #endregion

        #region ---------- Private fields ----------

        private PlcAddress address;
        private PlcRawType rawType;
        private WatchEntryType type;

        #endregion

        #region ---------- Public constructors ----------

        public WatchEntry()
            : base()
        {
            this.address = "DB1.DBX 1.0";
            this.rawType = PlcRawType.Bit;
            this.type = WatchEntryType.Bool;
        }

        #endregion

        #region ---------- Public properties ----------

        [Category("Entry")]
        [DefaultValue("DB1.DBX 1.0")]
        [Description("The address of the PLC data.")]
        public PlcAddress Address
        {
            get
            {
                return this.address;
            }

            set
            {
                if (this.address != value) {
                    this.address = value;

                    if (this.UpdateType())
                        this.RaisePropertyChanged("Type");

                    this.RaisePropertyChanged("Address");
                }
            }
        }

        [Browsable(false)]
        public PlcRawType RawType
        {
            get
            {
                return this.rawType;
            }

            private set
            {
                if (this.rawType != value) {
                    this.rawType = value;
                    this.RaisePropertyChanged("RawType");
                }
            }
        }

        [Category("Entry")]
        [DefaultValue(WatchEntryType.Bool)]
        [TypeConverter(typeof(EnumConverter<WatchEntryType>))]
        [Description("The type of the PLC data.")]
        public WatchEntryType Type
        {
            get
            {
                return this.type;
            }

            set
            {
                if (this.type != value) {
                    this.type = value;

                    if (this.UpdateAddress())
                        this.RaisePropertyChanged("Address");

                    this.RaisePropertyChanged("Type");
                }
            }
        }

        #endregion

        #region ---------- Public static methods ----------

        public static WatchEntry FromXElement(XElement element)
        {
            var entry = new WatchEntry();
            var addressAttribute = element.Attribute("Address");

            if (addressAttribute != null)
                entry.Address = PlcAddress.Parse(addressAttribute.Value);

            var typeAttribute = element.Attribute("Type");

            if (typeAttribute != null) {
                entry.Type = (WatchEntryType)Enum.Parse(
                        typeof(WatchEntryType), typeAttribute.Value);
            }

            return entry;
        }

        #endregion

        #region ---------- Public methods ----------

        public XElement ToXElement()
        {
            return new XElement(
                    WatchEntry.ElementName,
                    new XAttribute("Address", this.address),
                    new XAttribute("Type", this.type));
        }

        #endregion

        #region ---------- Private static methods ----------

        private static PlcRawType GetRawType(WatchEntryType type)
        {
            if (type == WatchEntryType.Bool)
                return PlcRawType.Bit;

            if (type == WatchEntryType.Byte
                    || type == WatchEntryType.Char
                    || type == WatchEntryType.DateTime
                    || type == WatchEntryType.String) {
                return PlcRawType.Byte;
            }

            if (type == WatchEntryType.Int
                    || type == WatchEntryType.Word
                    || type == WatchEntryType.Date) {
                return PlcRawType.Word;
            }

            if (type == WatchEntryType.DInt
                    || type == WatchEntryType.DWord
                    || type == WatchEntryType.Real
                    || type == WatchEntryType.Double
                    || type == WatchEntryType.Time
                    || type == WatchEntryType.TimeOfDay) {
                return PlcRawType.DWord;
            }

            throw new ArgumentException();
        }

        private static WatchEntryType GetType(PlcRawType rawType)
        {
            if (rawType == PlcRawType.Bit)
                return WatchEntryType.Bool;

            if (rawType == PlcRawType.Byte)
                return WatchEntryType.Byte;

            if (rawType == PlcRawType.Word)
                return WatchEntryType.Int;

            if (rawType == PlcRawType.DWord)
                return WatchEntryType.DInt;

            throw new ArgumentException();
        }

        #endregion

        #region ---------- Private methods ----------

        private bool UpdateAddress()
        {
            this.RawType = WatchEntry.GetRawType(this.type);

            if (this.address.RawType != this.rawType) {
                if (this.rawType == PlcRawType.Bit) {
                    this.address = new PlcAddress(
                            this.address.Operand, this.rawType, this.address.ByteNumber, 0);
                }
                else {
                    this.address = new PlcAddress(
                            this.address.Operand, this.rawType, this.address.ByteNumber);
                }

                return true;
            }

            return false;
        }

        private bool UpdateType()
        {
            this.RawType = WatchEntry.GetRawType(this.type);

            if (this.rawType != this.address.RawType) {
                this.type = WatchEntry.GetType(this.address.RawType);
                return true;
            }

            return false;
        }

        #endregion
    }
}

﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System.Collections.ObjectModel;

    public class WatchEntryCollection : Collection<WatchEntry>
    {
        #region ---------- Public constructors ----------

        public WatchEntryCollection()
            : base()
        {
        }

        #endregion
    }
}

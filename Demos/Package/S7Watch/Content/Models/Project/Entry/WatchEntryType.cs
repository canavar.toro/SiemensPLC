﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using IPS7Lnk.Advanced;
    using S7Watch.ComponentModel;

    public enum WatchEntryType : int
    {
        [EnumMemberName("Bool")]
        Bool = PlcTypeCode.Bool,

        [EnumMemberName("Byte")]
        Byte = PlcTypeCode.Byte,

        [EnumMemberName("Char")]
        Char = PlcTypeCode.Char,

        [EnumMemberName("Int (Int16)")]
        Int = PlcTypeCode.Int,

        [EnumMemberName("Word (UInt16)")]
        Word = PlcTypeCode.Word,

        [EnumMemberName("DInt (Int32)")]
        DInt = PlcTypeCode.DInt,

        [EnumMemberName("DWord (UInt32)")]
        DWord = PlcTypeCode.DWord,

        [EnumMemberName("Real")]
        Real = PlcTypeCode.Real,

        [EnumMemberName("Double")]
        Double = PlcTypeCode.Double,

        [EnumMemberName("Date")]
        Date = PlcTypeCode.Date,

        [EnumMemberName("DateTime")]
        DateTime = PlcTypeCode.DateTime,

        [EnumMemberName("Time")]
        Time = PlcTypeCode.Time,

        [EnumMemberName("TimeOfDay")]
        TimeOfDay = PlcTypeCode.TimeOfDay,

        [EnumMemberName("String[16]")]
        String = PlcTypeCode.String
    }
}

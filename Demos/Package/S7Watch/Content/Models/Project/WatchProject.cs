﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Xml.Linq;

    using S7Watch.ComponentModel;

    public class WatchProject : Observable
    {
        #region ---------- Public static fields ----------

        public static readonly string ElementName = "Project";

        #endregion

        #region ---------- Private fields ----------

        private WatchDevice device;
        private WatchEntryCollection entries;
        private int interval;

        #endregion

        #region ---------- Public constructors ----------

        public WatchProject()
            : base()
        {
            this.Device = new WatchDevice();

            this.entries = new WatchEntryCollection();
            this.interval = 500;
        }

        #endregion

        #region ---------- Public properties ----------

        [Category("Target")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [DisplayName("Device")]
        [Description("The PLC device information.")]
        public WatchDevice Device
        {
            get
            {
                return this.device;
            }

            private set
            {
                if (this.device != value) {
                    if (this.device != null)
                        this.device.PropertyChanged -= this.HandleDevicePropertyChanged;

                    this.device = value;

                    if (this.device != null)
                        this.device.PropertyChanged += this.HandleDevicePropertyChanged;

                    this.RaisePropertyChanged("Device");
                }
            }
        }

        [Browsable(false)]
        public WatchEntryCollection Entries
        {
            get
            {
                return this.entries;
            }
        }

        [Category("Watch")]
        [DefaultValue(500)]
        [DisplayName("Interval")]
        [Description("The interval in milliseconds used to automatically read/write the latest data.")]
        public int Interval
        {
            get
            {
                return this.interval;
            }

            set
            {
                if (value < 0)
                    value = 0;

                if (this.interval != value) {
                    this.interval = value;
                    this.RaisePropertyChanged("Interval");
                }
            }
        }

        #endregion

        #region ---------- Public static methods ----------

        public static WatchProject FromXElement(XElement element)
        {
            var project = new WatchProject();
            var intervalAttribute = element.Attribute("Interval");

            if (intervalAttribute != null)
                project.Interval = Convert.ToInt32(intervalAttribute.Value);

            var deviceElement = element.Element(WatchDevice.ElementName);

            if (deviceElement != null)
                project.Device = WatchDevice.FromXElement(deviceElement);

            var entriesElement = element.Element("Entries");

            if (entriesElement != null) {
                foreach (var entryElement in entriesElement.Elements())
                    project.entries.Add(WatchEntry.FromXElement(entryElement));
            }

            return project;
        }

        public static WatchProject Load(string filePath)
        {
            WatchProject project = null;

            if (File.Exists(filePath)) {
                var document = XDocument.Load(filePath);
                var projectElement = document.Element(WatchProject.ElementName);

                if (projectElement != null)
                    project = WatchProject.FromXElement(projectElement);
            }

            return project;
        }

        #endregion

        #region ---------- Public methods ----------

        public void Save(string filePath)
        {
            new XDocument(this.ToXElement()).Save(filePath);
        }

        public XElement ToXElement()
        {
            var entries = new XElement("Entries");

            foreach (var entry in this.entries)
                entries.Add(entry.ToXElement());

            return new XElement(
                    WatchProject.ElementName,
                    new XAttribute("Interval", this.interval),
                    this.device.ToXElement(),
                    entries);
        }

        #endregion

        #region ---------- Private methods ----------

        private void HandleDevicePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.RaisePropertyChanged(WatchDevice.ElementName + "." + e.PropertyName);
        }

        #endregion
    }
}

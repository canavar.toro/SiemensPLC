﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

namespace S7Watch
{
    using System;
    using System.ComponentModel;
    using System.Text;
    using System.Windows.Forms;

    using IPS7Lnk.Advanced;

    using S7Watch.ComponentModel;
    using S7Watch.Controls;

    using FormsApplication = System.Windows.Forms.Application;

    public partial class MainView : Form
    {
        #region ---------- Private fields ----------

        private WatchMonitor monitor;
        private WatchProject project;

        #endregion

        #region ---------- Public constructors ----------

        public MainView()
            : base()
        {
            this.InitializeComponent();
            this.Text = FormsApplication.ProductName;

            this.typeDataGridViewComboBoxColumn.DataSource = new EnumBindingList<WatchEntryType>();
            this.typeDataGridViewComboBoxColumn.DisplayMember = "Name";
            this.typeDataGridViewComboBoxColumn.ValueMember = "Value";

            this.statusDataGridViewImageColumn.CellTemplate = new DataGridViewNullImageCell();

            PlcNotifications.ConnectionOpened += this.HandlePlcNotificationsConnectionOpened;
            PlcNotifications.EvaluateStatus = this.EvaluateStatus;

            this.UpdateView(new WatchProject());
        }

        #endregion

        #region ---------- Protected methods ----------

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
                this.monitor.Stop();
        }

        #endregion

        #region ---------- Private methods ----------

        private bool EvaluateStatus(IPlcStatusProvider provider)
        {
            var status = "Ready";

            if (provider.Status.Code != PlcStatusCode.NoError) {
                var source = "General";
                var connection = provider as PlcDeviceConnection;

                if (connection != null)
                    source = "Connection";

                var value = provider as IPlcValue;

                if (value != null)
                    source = string.Format("Value '{0}'", value.Type.Address);

                status = string.Format("{0}: {1}", source, provider.Status.Text);
            }

            this.Invoke(() => this.statusToolStripStatusLabel.Text = status);

            // Ignore all kinds of errors (do not throw an exception).
            return true;
        }

        private string FormatToDual(WatchNode node)
        {
            var text = new StringBuilder();
            var bytes = node.GetValueBytes();

            for (int index = bytes.Length - 1; index >= 0; index--)
                text.Append(Convert.ToString(bytes[index], 2).PadLeft(8, '0'));

            var dualValue = text.ToString();

            dualValue = dualValue.PadLeft(
                    PlcRawTypes.GetSize(node.Entry.RawType, false), '0');

            return StringHelper.Partition(dualValue);
        }

        private string FormatToHex(WatchNode node)
        {
            var text = new StringBuilder();
            var bytes = node.GetValueBytes();

            for (int index = bytes.Length - 1; index >= 0; index--)
                text.Append(Convert.ToString(bytes[index], 16).PadLeft(8, '0'));

            var hexValue = text.ToString();

            hexValue = hexValue.PadLeft(
                    PlcRawTypes.GetSize(node.Entry.RawType, false), '0');

            return "0x" + StringHelper.Partition(hexValue).ToUpper();
        }

        private void HandleAutoUpdateToolStripButtonCheckedChanged(object sender, EventArgs e)
        {
            this.monitor.IsEnabled = this.autoUpdateToolStripButton.Checked;
        }

        private void HandleDataGridViewCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var row = this.monitorNodesDataGridView.Rows[e.RowIndex];
            var node = row.DataBoundItem as WatchNode;

            if (node != null)
                node.BeginEdit();
        }

        private void HandleDataGridViewCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var row = this.monitorNodesDataGridView.Rows[e.RowIndex];
            var node = row.DataBoundItem as WatchNode;

            if (node != null)
                node.EndEdit();
        }

        private void HandleDataGridViewCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var row = this.monitorNodesDataGridView.Rows[e.RowIndex];
            var node = row.DataBoundItem as WatchNode;

            if (node != null) {
                var columnIndex = e.ColumnIndex;

                if (columnIndex == this.statusDataGridViewImageColumn.Index) {
                    row.Cells[columnIndex].ToolTipText = node.StatusText;
                }
                else if (columnIndex == this.valueDataGridViewTextBoxColumn.Index) {
                    e.Value = node.Value.ToString();
                    e.FormattingApplied = true;
                }
                else if (columnIndex == this.dualValueDataGridViewTextBoxColumn.Index) {
                    e.Value = this.FormatToDual(node);
                    e.FormattingApplied = true;
                }
                else if (columnIndex == this.hexValueDataGridViewTextBoxColumn.Index) {
                    e.Value = this.FormatToHex(node);
                    e.FormattingApplied = true;
                }
            }
        }

        private void HandleOpenToolStripButtonClick(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.AddExtension = true;
            dialog.AutoUpgradeEnabled = true;
            dialog.CheckFileExists = true;
            dialog.DefaultExt = ".wproj";
            dialog.Filter = "Watch Project File (*.wproj)|*.wproj";
            dialog.Title = "Open Project";

            if (dialog.ShowDialog() == DialogResult.OK)
                this.UpdateView(WatchProject.Load(dialog.FileName));
        }

        private void HandlePlcNotificationsConnectionOpened(
                object sender,
                PlcNotifications.PlcDeviceConnectionEventArgs e)
        {
            this.UpdateStatus(string.Format(
                    "Monitoring - {0}.", e.Connection.Device.EndPoint));
        }

        private void HandleReadFromPlcToolStripButtonClick(object sender, EventArgs e)
        {
            this.Invoke(this.monitor.ReadValues);
        }

        private void HandleSaveToolStripButtonClick(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.AutoUpgradeEnabled = true;
            dialog.DefaultExt = ".wproj";
            dialog.FileName = "New Project";
            dialog.Filter = "Watch Project File (*.wproj)|*.wproj";
            dialog.OverwritePrompt = true;
            dialog.Title = "Save Project";

            if (dialog.ShowDialog() == DialogResult.OK)
                this.project.Save(dialog.FileName);
        }

        private void HandleShowAboutToolStripButtonClick(object sender, EventArgs e)
        {
            var view = new Form() {
                BackgroundImage = Resources.Info,
                ClientSize = Resources.Info.Size,
                ControlBox = false,
                FormBorderStyle = FormBorderStyle.FixedSingle,
                ShowInTaskbar = false,
                StartPosition = FormStartPosition.CenterParent
            };

            view.Click += (sender2, e2) => view.Close();
            view.ShowDialog();
        }

        private void HandleWriteToPlcToolStripButtonClick(object sender, EventArgs e)
        {
            this.Invoke(this.monitor.WriteValues);
        }

        private void Invoke(Action method)
        {
            if (this.IsHandleCreated)
                base.Invoke(method);
            else
                method();
        }

        private void UpdateStatus(string status)
        {
            this.Invoke(() => this.statusToolStripStatusLabel.Text = status);
        }

        private void UpdateView(WatchProject project)
        {
            this.project = project;

            this.projectPropertyGrid.SelectedObject = project;
            this.projectPropertyGrid.ExpandAllGridItems();

            this.monitor = new WatchMonitor(project);
            this.monitor.SynchronizeInvoke = this;
            this.monitor.IsEnabled = this.autoUpdateToolStripButton.Checked;

            this.monitorNodesBindingSource.DataSource = this.monitor.Nodes;
        }

        #endregion


    }
}

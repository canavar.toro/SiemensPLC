﻿// Copyright (c) Traeger Industry Components GmbH.  All Rights Reserved.

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("S7 Watch")]
[assembly: AssemblyDescription("Traeger S7 Watch")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Traeger Industry Components GmbH")]
[assembly: AssemblyProduct("S7 Watch")]
[assembly: AssemblyCopyright("Copyright © 2013-2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Settings used when the assembly is exposed to COM.
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM.
[assembly: Guid("39A7C98B-7D67-4DED-AA04-65769D0D865D")]

// Version information for the assembly.
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: AssemblyFileVersion("1.0.1.0")]
